const path = require("path");
const HtmlWebpackPlugin = require('html-webpack-plugin')

const resolveDir = dir => '../../' + dir

module.exports = {
  mode: process.env.NODE_ENV === 'production' ? "production" : "development",
  plugins: [
    new HtmlWebpackPlugin({
      template: './src/index.html',
      title: 'Output Management'
    })
  ],
  output: {
    path: path.join(__dirname, resolveDir("dist/app")),
    filename: "[name].bundle.js",
    chunkFilename: "[id].chunk.js",
    publicPath: '/'
  },
  resolve: {
    extensions: [ '.tsx', '.ts', '.js' ]
  },
  module: {
    rules: [
      {
        test: /\.tsx?$/,
        loader: 'ts-loader',
        exclude: /node_modules/
      }, 
      {
        test: /\.(png|jpg|gif|bsp|lmp)$/i,
        use: [
          {
            loader: 'url-loader',
            options: {
              limit: 8192,
            },
          },
        ],
      }
    ]
  }
}