const base = require('./webpack.config')
const merge = require('webpack-merge')
const webpack = require('webpack')

module.exports = merge({
  devtool: 'eval-source-map',
  devServer: {
    compress: true,
    port: 9000,
    hot: true
  },
  entry: {
    app: ['webpack-hot-middleware/client?reload=true', "./src/index.ts"]
  },
  plugins: [
    new webpack.HotModuleReplacementPlugin()
  ],
}, base)