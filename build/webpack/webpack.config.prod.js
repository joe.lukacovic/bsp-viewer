const base = require('./webpack.config')
const merge = require('webpack-merge')

module.exports = merge({
  devtool: 'cheap-source-map',
  experiments: {
    outputModule: true,
  },
  entry: {
    viewer: [ "./src/lib.ts"]
  },
  plugins: [
  ], 
  output: {
    library: {
      type: 'module'
    }
  },
}, base)