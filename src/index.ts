import {init as initMain} from './engine/main'
import {dom} from './dom'

const ourDoc = dom(window.document)
const setSize = () => {
  
	var width = (ourDoc.docEl.clientWidth <= 320) ? 320 : ourDoc.docEl.clientWidth;
	var height = (ourDoc.docEl.clientHeight <= 200) ? 200 : ourDoc.docEl.clientHeight;

  ourDoc.canvas.width = width
  ourDoc.canvas.height = height
}

window.addEventListener('resize', setSize)

setSize()

initMain(ourDoc, null, null, {
  showOverlay: true, 
  sampleInput: true
})