import {angleVectors, rotatePointAroundVector, dotProduct, boxOnPlaneSide} from './vector'
import {model, pointInLeaf, leafPVS} from './bsp'
import {programs, init as initPrograms, useProgram, unbindProgram} from './program'
import {CONTENTS, identity} from './common'
import {bind} from './textures'
import {dom} from '../glstate'
import {state as txState} from './textures'
import {entities} from './entities'
import { stat } from 'fs';
import {init as initCamera, setOrigin, setAngles, state as cameraState} from './camera'

const LIGHTMAP_DIM = 1024
const state = {
	oldtime: 0.0,
	realtime: 0.0,
	viewEnt: {
		origin:  [0.0, 0.0, 0.0],
		angles:  [0.0, 0.0, 0.0]
	},
	visframecount: 0,
	frustum: [{normal: []}, {normal: []}, {normal: []}, {normal: []}],
	vup: [],
	vpn: [],
	vright: [],
	fov: 110,
	refdef: {
		vrect: {},
		vieworg: [0.0, 0.0, 0.0],
		viewangles: [0.0, 0.0, 0.0],
		fov_x: 0,
		fov_y: 0
	},
	c_brush_verts: 0,
	c_alias_polys: 0,
	drawsky: false,
	viewleaf: {} as any,
	oldviewleaf: null as any,
	perspective: [
		0.0, 0.0, 0.0, 0.0,
		0.0, 0.0, 0.0, 0.0,
		0.0, 0.0, -65540.0 / 65532.0, -1.0,
		0.0, 0.0, -524288.0 / 65532.0, 0.0
	],
	
	lightmaps: new Uint8Array(new ArrayBuffer(4096 * LIGHTMAP_DIM)),
	lightstylevalue: [],
	// settings
	gamma: 1,
	skyvecs: null
}

export const init = (gl: WebGLRenderingContext) => {

	state.oldtime = Date.now() * 0.001;
	state.viewEnt = entities.find(ent => ent.classname === 'info_player_start')
	//state.viewEnt.origin = [-916.1644276287493, 435.0361505074511, 54.03125]
	state.viewEnt.angles = [0, 195, 0]
	state.visframecount = 0;

	state.frustum = [{normal: []}, {normal: []}, {normal: []}, {normal: []}];

	state.vup = [];
	state.vpn = [];
	state.vright = [];
	state.c_brush_verts = 0;
	state.c_alias_polys = 0;
	state.fov =  110;
	state.refdef = {
		vrect: {},
		vieworg: [0.0, 0.0, 0.0],
		viewangles: [0.0, 0.0, 0.0],
		fov_x: 0,
		fov_y: 0
	}
	state.viewleaf = 0
	state.gamma = 1
	state.perspective = [
		0.0, 0.0, 0.0, 0.0,
		0.0, 0.0, 0.0, 0.0,
		0.0, 0.0, -65540.0 / 65532.0, -1.0,
		0.0, 0.0, -524288.0 / 65532.0, 0.0
	];
	
	state.lightstylevalue = []
	initPrograms(gl)
	
	const lm_allocated = [];
	for (var i = 0; i < LIGHTMAP_DIM; ++i)
		lm_allocated[i] = 0;
	for (var j = 0; j < model.faces.length; ++j)
	{
		var surf = model.faces[j];
		if ((surf.sky !== true) && (surf.turbulent !== true))
		{
			allocBlock(lm_allocated, surf);
			if (model.lightdata != null)
				buildLightMap(model, surf);
		}
		buildSurfaceDisplayList(model, surf);
	}
	makeWorldModelDisplayLists(gl, model)
	bind(gl, 0, txState.lightmap_texture);
	gl.texImage2D(gl.TEXTURE_2D, 0, gl.RGBA, LIGHTMAP_DIM, LIGHTMAP_DIM, 0, gl.RGBA, gl.UNSIGNED_BYTE, state.lightmaps);
	initCamera()
	makeSky(gl)
	setOrigin(state.viewEnt.origin)
	setAngles(state.viewEnt.angles)
}

const allocBlock = (allocated, surf) => {
	var w = (surf.extents[0] >> 4) + 1, h = (surf.extents[1] >> 4) + 1;
	var x, y, i, j, best = LIGHTMAP_DIM, best2;
	for (i = 0; i < (LIGHTMAP_DIM - w); ++i)
	{
		best2 = 0;
		for (j = 0; j < w; ++j)
		{
			if (allocated[i + j] >= best)
				break;
			if (allocated[i + j] > best2)
				best2 = allocated[i + j];
		}
		if (j === w)
		{
			x = i;
			y = best = best2;
		}
	}
	best += h;
	if (best > LIGHTMAP_DIM)
		return
		//sys.error('AllocBlock: full');
	for (i = 0; i < w; ++i)
		allocated[x + i] = best;
	surf.light_s = x;
	surf.light_t = y;
}

export const buildLightMap = (model, surf) =>
{
	var dest;
	var smax = (surf.extents[0] >> 4) + 1;
	var tmax = (surf.extents[1] >> 4) + 1;
	var i, j;
	var lightmap = surf.lightofs;
	var maps;
	for (maps = 0; maps < surf.styles.length; ++maps)
	{
		dest = (surf.light_t << 12) + (surf.light_s << 2) + maps;
		for (i = 0; i < tmax; ++i)
		{
			for (j = 0; j < smax; ++j)
				state.lightmaps[dest + (j << 2)] = model.lightdata[lightmap + j];
			lightmap += smax;
			dest += 4096;
		}
	}
	for (; maps <= 3; ++maps)
	{
		dest = (surf.light_t << 12) + (surf.light_s << 2) + maps;
		for (i = 0; i < tmax; ++i)
		{
			for (j = 0; j < smax; ++j)
				state.lightmaps[dest + (j << 2)] = 0;
			dest += 4096;
		}
	}
}

// Based on Quake 2 polygon generation algorithm by Toji - http://blog.tojicode.com/2010/06/quake-2-bsp-quite-possibly-worst-format.html
export const buildSurfaceDisplayList = (model, fa) => {
	fa.verts = [];
	if (fa.numedges <= 2)
		return;
	var i, index, _vec, vert, s, t;
	var texinfo = model.texinfo[fa.texinfo];
	var texture = model.textures[texinfo.texture];
	for (i = 0; i < fa.numedges; ++i)
	{
		index = model.surfedges[fa.firstedge + i];
		if (index > 0)
			_vec = model.vertexes[model.edges[index][0]];
		else
			_vec = model.vertexes[model.edges[-index][1]];
		vert = [_vec[0], _vec[1], _vec[2]];
		if (fa.sky !== true)
		{
			s = dotProduct(_vec, texinfo.vecs[0]) + texinfo.vecs[0][3];
			t = dotProduct(_vec, texinfo.vecs[1]) + texinfo.vecs[1][3];
			vert[3] = s / texture.width;
			vert[4] = t / texture.height;
			if (fa.turbulent !== true)
			{
				vert[5] = (s - fa.texturemins[0] + (fa.light_s << 4) + 8.0) / 16384.0;
				vert[6] = (t - fa.texturemins[1] + (fa.light_t << 4) + 8.0) / 16384.0;
			}
		}
		if (i >= 3)
		{
			fa.verts[fa.verts.length] = fa.verts[0];
			fa.verts[fa.verts.length] = fa.verts[fa.verts.length - 2];
		}
		fa.verts[fa.verts.length] = vert;
	}
}

const makeWorldModelDisplayLists = (gl, m) => {
	if (m.cmds != null)
		return;
	var i, j, k, l;
	var cmds = [];
	var texture, leaf, chain, surf, vert, styles = [0.0, 0.0, 0.0, 0.0];
	var verts = 0;
	for (i = 0; i < m.textures.length; ++i)
	{
		texture = m.textures[i];
		if ((texture.sky === true) || (texture.turbulent === true))
			continue;
		for (j = 0; j < m.leafs.length; ++j)
		{
			leaf = m.leafs[j];
			chain = [i, verts, 0];
			for (k = 0; k < leaf.nummarksurfaces; ++k)
			{
				surf = m.faces[m.marksurfaces[leaf.firstmarksurface + k]];
				if (surf.texture !== i)
					continue;
				styles[0] = styles[1] = styles[2] = styles[3] = 0.0;
				switch (surf.styles.length)
				{
				case 4:
					styles[3] = surf.styles[3] * 0.015625 + 0.0078125;
				case 3:
					styles[2] = surf.styles[2] * 0.015625 + 0.0078125;
				case 2:
					styles[1] = surf.styles[1] * 0.015625 + 0.0078125;
				case 1:
					styles[0] = surf.styles[0] * 0.015625 + 0.0078125;
				}
				chain[2] += surf.verts.length;
				for (l = 0; l < surf.verts.length; ++l)
				{
					vert = surf.verts[l];
					cmds[cmds.length] = vert[0];
					cmds[cmds.length] = vert[1];
					cmds[cmds.length] = vert[2];
					cmds[cmds.length] = vert[3];
					cmds[cmds.length] = vert[4];
					cmds[cmds.length] = vert[5];
					cmds[cmds.length] = vert[6];
					cmds[cmds.length] = styles[0];
					cmds[cmds.length] = styles[1];
					cmds[cmds.length] = styles[2];
					cmds[cmds.length] = styles[3];
				}
			}
			if (chain[2] !== 0)
			{
				leaf.cmds[leaf.cmds.length] = chain;
				++leaf.skychain;
				++leaf.waterchain;
				verts += chain[2];
			}
		}
	}
	m.skychain = verts * 44;
	verts = 0;
	for (i = 0; i < m.textures.length; ++i)
	{
		texture = m.textures[i];
		if (texture.sky !== true)
			continue;
		for (j = 0; j < m.leafs.length; ++j)
		{
			leaf = m.leafs[j];
			chain = [verts, 0];
			for (k = 0; k < leaf.nummarksurfaces; ++k)
			{
				surf = m.faces[m.marksurfaces[leaf.firstmarksurface + k]];
				if (surf.texture !== i)
					continue;
				chain[1] += surf.verts.length;
				for (l = 0; l < surf.verts.length; ++l)
				{
					vert = surf.verts[l];
					cmds[cmds.length] = vert[0];
					cmds[cmds.length] = vert[1];
					cmds[cmds.length] = vert[2];
				}
			}
			if (chain[1] !== 0)
			{
				leaf.cmds[leaf.cmds.length] = chain;
				++leaf.waterchain;
				verts += chain[1];
			}
		}
	}
	m.waterchain = m.skychain + verts * 12;
	verts = 0;
	for (i = 0; i < m.textures.length; ++i)
	{
		texture = m.textures[i];
		if (texture.turbulent !== true)
			continue;
		for (j = 0; j < m.leafs.length; ++j)
		{
			leaf = m.leafs[j];
			chain = [i, verts, 0];
			for (k = 0; k < leaf.nummarksurfaces; ++k)
			{
				surf = m.faces[m.marksurfaces[leaf.firstmarksurface + k]];
				if (surf.texture !== i)
					continue;
				chain[2] += surf.verts.length;
				for (l = 0; l < surf.verts.length; ++l)
				{
					vert = surf.verts[l];
					cmds[cmds.length] = vert[0];
					cmds[cmds.length] = vert[1];
					cmds[cmds.length] = vert[2];
					cmds[cmds.length] = vert[3];
					cmds[cmds.length] = vert[4];
				}
			}
			if (chain[2] !== 0)
			{
				leaf.cmds[leaf.cmds.length] = chain;
				verts += chain[2];
			}
		}
	}
	m.cmds = gl.createBuffer();
	gl.bindBuffer(gl.ARRAY_BUFFER, m.cmds);
	gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(cmds), gl.STATIC_DRAW);
};

const setFov = (gl) => {
	if ((dom.canvas.width * 0.75) <= dom.canvas.height)
	{
		state.refdef.fov_x = state.fov;
		state.refdef.fov_y = Math.atan(dom.canvas.height / (dom.canvas.width / Math.tan(state.fov * Math.PI / 360.0))) * 360.0 / Math.PI;
	}
	else
	{
		state.refdef.fov_x = Math.atan(dom.canvas.width / (dom.canvas.height / Math.tan(state.fov * 0.82 * Math.PI / 360.0))) * 360.0 / Math.PI;
		state.refdef.fov_y = state.fov * 0.82;
	}
}
const setFrustum = () => {
	state.frustum[0].normal = rotatePointAroundVector(state.vup, state.vpn, -(90.0 - state.refdef.fov_x * 0.5));
	state.frustum[1].normal = rotatePointAroundVector(state.vup, state.vpn, 90.0 - state.refdef.fov_x * 0.5);
	state.frustum[2].normal = rotatePointAroundVector(state.vright, state.vpn, 90.0 - state.refdef.fov_y * 0.5);
	state.frustum[3].normal = rotatePointAroundVector(state.vright, state.vpn, -(90.0 - state.refdef.fov_y * 0.5));
	var i, out;
	for (i = 0; i <= 3; ++i)
	{
		out = state.frustum[i];
		out.type = 5;
		out.dist = dotProduct(state.refdef.vieworg, out.normal);
		out.signbits = 0;
		if (out.normal[0] < 0.0)
			out.signbits = 1;
		if (out.normal[1] < 0.0)
			out.signbits += 2;
		if (out.normal[2] < 0.0)
			out.signbits += 4;
		if (out.normal[3] < 0.0)
			out.signbits += 8;
	}
}

const perspective = (gl) => {
	var viewangles = [
		state.refdef.viewangles[0] * Math.PI / 180.0,
		(state.refdef.viewangles[1] - 90.0) * Math.PI / -180.0,
		state.refdef.viewangles[2] * Math.PI / -180.0
	];
	var sp = Math.sin(viewangles[0]);
	var cp = Math.cos(viewangles[0]);
	var sy = Math.sin(viewangles[1]);
	var cy = Math.cos(viewangles[1]);
	var sr = Math.sin(viewangles[2]);
	var cr = Math.cos(viewangles[2]);
	var viewMatrix = [
		cr * cy + sr * sp * sy,		cp * sy,	-sr * cy + cr * sp * sy,
		cr * -sy + sr * sp * cy,	cp * cy,	-sr * -sy + cr * sp * cy,
		sr * cp,					-sp,		cr * cp
	];

	// if (v.cvr.gamma.value < 0.5)
	// 	cvar.setValue('gamma', 0.5);
	// else if (v.cvr.gamma.value > 1.0)
	// 	cvar.setValue('gamma', 1.0);

	// GL.unbindProgram();
	
	var ymax = 4.0 * Math.tan(state.refdef.fov_y * Math.PI / 360.0);
	state.perspective[0] = 4.0 / (ymax * dom.canvas.width / dom.canvas.height);
	state.perspective[5] = 4.0 / ymax;
  var i, program;
	for (i = 0; i < programs.length; ++i)
	{
		program = programs[i];
		gl.useProgram(program.program);
		if (program.uViewOrigin != null)
			gl.uniform3fv(program.uViewOrigin, state.refdef.vieworg);
		if (program.uViewAngles != null)
			gl.uniformMatrix3fv(program.uViewAngles, false, viewMatrix);
		if (program.uPerspective != null)
			gl.uniformMatrix4fv(program.uPerspective, false, state.perspective);
		if (program.uGamma != null)
			gl.uniform1f(program.uGamma, state.gamma);
	}
}

const recursiveLightPoint = function(node, start, end) {
	if (node.contents < 0)
		return -1;

	var normal = node.plane.normal;
	var front = start[0] * normal[0] + start[1] * normal[1] + start[2] * normal[2] - node.plane.dist;
	var back = end[0] * normal[0] + end[1] * normal[1] + end[2] * normal[2] - node.plane.dist;
	var side = front < 0;

	if ((back < 0) === side)
		return recursiveLightPoint(node.children[side === true ? 1 : 0], start, end);

	var frac = front / (front - back);
	var mid = [
		start[0] + (end[0] - start[0]) * frac,
		start[1] + (end[1] - start[1]) * frac,
		start[2] + (end[2] - start[2]) * frac
	];

	var r = recursiveLightPoint(node.children[side === true ? 1 : 0], start, mid);
	if (r >= 0)
		return r;

	if ((back < 0) === side)
		return -1;

	var i, surf, tex, s, t, ds, dt, lightmap, size, maps;
	for (i = 0; i < node.numfaces; ++i)
	{
		surf = model.faces[node.firstface + i];
		if ((surf.sky === true) || (surf.turbulent === true))
			continue;

		tex = model.texinfo[surf.texinfo];

		s = dotProduct(mid, tex.vecs[0]) + tex.vecs[0][3];
		t = dotProduct(mid, tex.vecs[1]) + tex.vecs[1][3];
		if ((s < surf.texturemins[0]) || (t < surf.texturemins[1]))
			continue;

		ds = s - surf.texturemins[0];
		dt = t - surf.texturemins[1];
		if ((ds > surf.extents[0]) || (dt > surf.extents[1]))
			continue;

		if (surf.lightofs === 0)
			return 0;

		ds >>= 4;
		dt >>= 4;

		lightmap = surf.lightofs;
		if (lightmap === 0)
			return 0;

		lightmap += dt * ((surf.extents[0] >> 4) + 1) + ds;
		r = 0;
		size = ((surf.extents[0] >> 4) + 1) * ((surf.extents[1] >> 4) + 1);
		for (maps = 0; maps < surf.styles.length; ++maps)
		{
			r += model.lightdata[lightmap] * state.lightstylevalue[surf.styles[maps]] * 22;
			lightmap += size;
		}
		return r >> 8;
	}
	return recursiveLightPoint(node.children[side !== true ? 1 : 0], mid, end);
}

const recursiveWorldNode = (node) => {
	if (node.contents === CONTENTS.solid)
		return;
	if (node.contents < 0)
	{
		if (node.markvisframe !== state.visframecount)
			return;
		node.visframe = state.visframecount;
		if (node.skychain !== node.waterchain)
			state.drawsky = true;
		return;
	}
	recursiveWorldNode(node.children[0]);
	recursiveWorldNode(node.children[1]);
}

const markLeaves = function()
{
	if (state.oldviewleaf === state.viewleaf)
		return;
	++state.visframecount;
	state.oldviewleaf = state.viewleaf;
	var vis = leafPVS(state.viewleaf, model);
	var i, node;
	for (i = 0; i < model.leafs.length; ++i)
	{
		if ((vis[i >> 3] & (1 << (i & 7))) === 0)
			continue;
		for (node = model.leafs[i + 1]; node != null; node = node.parent)
		{
			if (node.markvisframe === state.visframecount)
				break;
			node.markvisframe = state.visframecount;
		}
	}
	do
	{
		var p = [state.refdef.vieworg[0], state.refdef.vieworg[1], state.refdef.vieworg[2]];
		var leaf;
		if (state.viewleaf.contents <= CONTENTS.water)
		{
			leaf = pointInLeaf([state.refdef.vieworg[0], state.refdef.vieworg[1], state.refdef.vieworg[2] + 16.0], model);
			if (leaf.contents <= CONTENTS.water)
				break;
		}
		else
		{
			leaf = pointInLeaf([state.refdef.vieworg[0], state.refdef.vieworg[1], state.refdef.vieworg[2] - 16.0], model);
			if (leaf.contents > CONTENTS.water)
				break;
		}
		if (leaf === state.viewleaf)
			break;
		vis = leafPVS(leaf, model);
		for (i = 0; i < model.leafs.length; ++i)
		{
			if ((vis[i >> 3] & (1 << (i & 7))) === 0)
				continue;
			for (node = model.leafs[i + 1]; node != null; node = node.parent)
			{
				if (node.markvisframe === state.visframecount)
					break;
				node.markvisframe = state.visframecount;
			}
		}
	} while (false);
	state.drawsky = false;
	recursiveWorldNode(model.nodes[0]);
};

const setupGL = (gl) =>
{
	// if (state.dowarp === true)
	// {
	// 	gl.bindFramebuffer(gl.FRAMEBUFFER, state.warpbuffer);
	// 	gl.clear(gl.COLOR_BUFFER_BIT + gl.DEPTH_BUFFER_BIT);
	// 	gl.viewport(0, 0, state.warpwidth, state.warpheight);
	// }
	// else
	// {
		// var vrect = state.refdef.vrect;
		// var pixelRatio = 1// scr.state.devicePixelRatio;
		// ASSUME viewport is correct.
		// gl.viewport((vrect.x * pixelRatio) >> 0, ((vid.state.height - vrect.height - vrect.y) * pixelRatio) >> 0, (vrect.width * pixelRatio) >> 0, (vrect.height * pixelRatio) >> 0);
	//}
	perspective(gl);
	gl.enable(gl.DEPTH_TEST);
};

export const makeSky = function(gl: WebGLRenderingContext)
{
	var sin = [0.0, 0.19509, 0.382683, 0.55557, 0.707107, 0.831470, 0.92388, 0.980785, 1.0];
	var vecs = [], i, j;

	for (i = 0; i < 7; i += 2)
	{
		vecs = vecs.concat(
		[
			0.0, 0.0, 1.0,
			sin[i + 2] * 0.19509, sin[6 - i] * 0.19509, 0.980785,
			sin[i] * 0.19509, sin[8 - i] * 0.19509, 0.980785
		]);
		for (j = 0; j < 7; ++j)
		{
			vecs = vecs.concat(
			[
				sin[i] * sin[8 - j], sin[8 - i] * sin[8 - j], sin[j],
				sin[i] * sin[7 - j], sin[8 - i] * sin[7 - j], sin[j + 1],
				sin[i + 2] * sin[7 - j], sin[6 - i] * sin[7 - j], sin[j + 1],

				sin[i] * sin[8 - j], sin[8 - i] * sin[8 - j], sin[j],
				sin[i + 2] * sin[7 - j], sin[6 - i] * sin[7 - j], sin[j + 1],
				sin[i + 2] * sin[8 - j], sin[6 - i] * sin[8 - j], sin[j]
			]);
		}
	}

	// GL.createProgram('Sky',
	// 	['uViewAngles', 'uPerspective', 'uScale', 'uGamma', 'uTime'],
	// 	[['aPosition', gl.FLOAT, 3]],
	// 	['tSolid', 'tAlpha']);
	// GL.createProgram('SkyChain',
	// 	['uViewOrigin', 'uViewAngles', 'uPerspective'],
	// 	[['aPosition', gl.FLOAT, 3]],
	// 	[]);

	state.skyvecs = gl.createBuffer();
	gl.bindBuffer(gl.ARRAY_BUFFER, state.skyvecs);
	gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(vecs), gl.STATIC_DRAW);
};

export const drawSkyBox = (gl: WebGLRenderingContext) => {
	if (state.drawsky !== true)
		return;

	gl.colorMask(false, false, false, false);
	var clmodel = model
	var program = useProgram(gl, 'SkyChain');
	gl.bindBuffer(gl.ARRAY_BUFFER, model.cmds);
	gl.vertexAttribPointer(program.aPosition.location, 3, gl.FLOAT, false, 12, clmodel.skychain);
	var i, j, leaf, cmds;
	for (i = 0; i < clmodel.leafs.length; ++i)
	{
		leaf = clmodel.leafs[i];
		if ((leaf.visframe !== state.visframecount) || (leaf.skychain === leaf.waterchain))
			continue;
		if (cullBox(leaf.mins, leaf.maxs) === true)
			continue;
		for (j = leaf.skychain; j < leaf.waterchain; ++j)
		{
			cmds = leaf.cmds[j];
			gl.drawArrays(gl.TRIANGLES, cmds[0], cmds[1]);
		}
	}
	gl.colorMask(true, true, true, true);

	gl.depthFunc(gl.GREATER);
	gl.depthMask(false);
	gl.disable(gl.CULL_FACE);

	program = useProgram(gl, 'Sky');
	gl.uniform2f(program.uTime, (state.realtime * 0.125) % 1.0, (state.realtime * 0.03125) % 1.0);
	bind(gl, program.tSolid, txState.solidskytexture);
	bind(gl, program.tAlpha, txState.alphaskytexture);
	gl.bindBuffer(gl.ARRAY_BUFFER, state.skyvecs);
	gl.vertexAttribPointer(program.aPosition.location, 3, gl.FLOAT, false, 12, 0);

	gl.uniform3f(program.uScale, 2.0, -2.0, 1.0);
	gl.drawArrays(gl.TRIANGLES, 0, 180);
	gl.uniform3f(program.uScale, 2.0, -2.0, -1.0);
	gl.drawArrays(gl.TRIANGLES, 0, 180);

	gl.uniform3f(program.uScale, 2.0, 2.0, 1.0);
	gl.drawArrays(gl.TRIANGLES, 0, 180);
	gl.uniform3f(program.uScale, 2.0, 2.0, -1.0);
	gl.drawArrays(gl.TRIANGLES, 0, 180);

	gl.uniform3f(program.uScale, -2.0, -2.0, 1.0);
	gl.drawArrays(gl.TRIANGLES, 0, 180);
	gl.uniform3f(program.uScale, -2.0, -2.0, -1.0);
	gl.drawArrays(gl.TRIANGLES, 0, 180);

	gl.uniform3f(program.uScale, -2.0, 2.0, 1.0);
	gl.drawArrays(gl.TRIANGLES, 0, 180);
	gl.uniform3f(program.uScale, -2.0, 2.0, -1.0);
	gl.drawArrays(gl.TRIANGLES, 0, 180);

	gl.enable(gl.CULL_FACE);
	gl.depthMask(true);
	gl.depthFunc(gl.LESS);
}


export const calcRefdef = () => {

	state.refdef.vieworg[0] = cameraState.origin[0] + 0.03125;
	state.refdef.vieworg[1] = cameraState.origin[1] + 0.03125;
	state.refdef.vieworg[2] = cameraState.origin[2] + 0.03125;

	state.refdef.viewangles[0] = cameraState.angles[0]
	state.refdef.viewangles[1] = cameraState.angles[1]
	state.refdef.viewangles[2] = cameraState.angles[2]

	// var ipitch = cvr.idlescale.value * Math.sin(cl.clState.time * cvr.ipitch_cycle.value) * cvr.ipitch_level.value;
	// var iyaw = cvr.idlescale.value * Math.sin(cl.clState.time * cvr.iyaw_cycle.value) * cvr.iyaw_level.value;
	// var iroll = cvr.idlescale.value * Math.sin(cl.clState.time * cvr.iroll_cycle.value) * cvr.iroll_level.value;
	// r.state.refdef.viewangles[0] += ipitch;
	// r.state.refdef.viewangles[1] += iyaw;
	// r.state.refdef.viewangles[2] += iroll;

	// var forward = [], right = [], up = [];
	// vec.angleVectors([-ent.angles[0], ent.angles[1], ent.angles[2]], forward, right, up);
	// r.state.refdef.vieworg[0] += cvr.ofsx.value * forward[0] + cvr.ofsy.value * right[0] + cvr.ofsz.value * up[0];
	// r.state.refdef.vieworg[1] += cvr.ofsx.value * forward[1] + cvr.ofsy.value * right[1] + cvr.ofsz.value * up[1];
	// r.state.refdef.vieworg[2] += cvr.ofsx.value * forward[2] + cvr.ofsy.value * right[2] + cvr.ofsz.value * up[2];

	// if (r.state.refdef.vieworg[0] < (ent.origin[0] - 14.0))
	// 	r.state.refdef.vieworg[0] = ent.origin[0] - 14.0;
	// else if (r.state.refdef.vieworg[0] > (ent.origin[0] + 14.0))
	// 	r.state.refdef.vieworg[0] = ent.origin[0] + 14.0;
	// if (r.state.refdef.vieworg[1] < (ent.origin[1] - 14.0))
	// 	r.state.refdef.vieworg[1] = ent.origin[1] - 14.0;
	// else if (r.state.refdef.vieworg[1] > (ent.origin[1] + 14.0))
	// 	r.state.refdef.vieworg[1] = ent.origin[1] + 14.0;
	// if (r.state.refdef.vieworg[2] < (ent.origin[2] - 22.0))
	// 	r.state.refdef.vieworg[2] = ent.origin[2] - 22.0;
	// else if (r.state.refdef.vieworg[2] > (ent.origin[2] + 30.0))
	// 	r.state.refdef.vieworg[2] = ent.origin[2] + 30.0;

	// var view = cl.clState.viewent;
	// view.angles[0] = -r.state.refdef.viewangles[0] - ipitch;
	// view.angles[1] = r.state.refdef.viewangles[1] - iyaw;
	// view.angles[2] = cl.clState.viewangles[2] - iroll;
	// view.origin[0] = ent.origin[0] + forward[0] * bob * 0.4;
	// view.origin[1] = ent.origin[1] + forward[1] * bob * 0.4;
	// view.origin[2] = ent.origin[2] + cl.clState.viewheight + forward[2] * bob * 0.4 + bob;
	// switch (scr.cvr.viewsize.value)
	// {
	// case 110:
	// case 90:
	// 	view.origin[2] += 1.0;
	// 	break;
	// case 100:
	// 	view.origin[2] += 2.0;
	// 	break;
	// case 80:
	// 	view.origin[2] += 0.5;
	// }
	// view.model = cl.clState.model_precache[cl.clState.stats[def.STAT.weapon]];
	// view.frame = cl.clState.stats[def.STAT.weaponframe];

	// r.state.refdef.viewangles[0] += cl.clState.punchangle[0];
	// r.state.refdef.viewangles[1] += cl.clState.punchangle[1];
	// r.state.refdef.viewangles[2] += cl.clState.punchangle[2];

	// if ((cl.clState.onground === true) && ((ent.origin[2] - oldz) > 0.0))
	// {
	// 	var steptime = cl.clState.time - cl.clState.oldtime;
	// 	if (steptime < 0.0)
	// 		steptime = 0.0;
	// 	oldz += steptime * 80.0;
	// 	if (oldz > ent.origin[2])
	// 		oldz = ent.origin[2];
	// 	else if ((ent.origin[2] - oldz) > 12.0)
	// 		oldz = ent.origin[2] - 12.0;
	// 	r.state.refdef.vieworg[2] += oldz - ent.origin[2];
	// 	view.origin[2] += oldz - ent.origin[2];
	// }
	// else
	// 	oldz = ent.origin[2];
	// if (chase.cvr.active.value !== 0)
	// 	chase.update();
};

const cullBox = (mins, maxs) => {
	if (boxOnPlaneSide(mins, maxs, state.frustum[0]) === 2)
		return true;
	if (boxOnPlaneSide(mins, maxs, state.frustum[1]) === 2)
		return true;
	if (boxOnPlaneSide(mins, maxs, state.frustum[2]) === 2)
		return true;
	if (boxOnPlaneSide(mins, maxs, state.frustum[3]) === 2)
		return true;
};


export const textureAnimation = function(base)
{
	var frame = 0;
	if (base.anim_base != null)
	{
		frame = base.anim_frame;
		base = model.textures[base.anim_base];
	}
	var anims = base.anims;
	if (anims == null)
		return base;
	return model.textures[anims[(Math.floor(/*TODO: TIME cl.clState.time */1 * 5.0) + frame) % anims.length]];
}

const drawWorld = (gl: WebGLRenderingContext) => {
	//state.currententity = cl.state.entities[0];
	gl.bindBuffer(gl.ARRAY_BUFFER, model.cmds);

	var program = useProgram(gl, 'Brush');
	gl.uniform3f(program.uOrigin, 0.0, 0.0, 0.0);
	gl.uniformMatrix3fv(program.uAngles, false, identity);
	gl.vertexAttribPointer(program.aPosition.location, 3, gl.FLOAT, false, 44, 0);
	gl.vertexAttribPointer(program.aTexCoord.location, 4, gl.FLOAT, false, 44, 12);
	gl.vertexAttribPointer(program.aLightStyle.location, 4, gl.FLOAT, false, 44, 28);

	bind(gl, program.tLightmap, txState.lightmap_texture);
	//bind(gl, program.tLightmap, txState.fullbright_texture);
	// bind(gl, program.tDlight, txState.null_texture);
	// bind(gl, program.tLightStyle, txState.lightstyle_texture);
	var i, j, leaf, cmds;
	for (i = 0; i < model.leafs.length; ++i)
	{
		leaf = model.leafs[i];
		if ((leaf.visframe !== state.visframecount) || (leaf.skychain === 0))
			continue;
		if (cullBox(leaf.mins, leaf.maxs) === true)
			continue;
		for (j = 0; j < leaf.skychain; ++j)
		{
			cmds = leaf.cmds[j];
			state.c_brush_verts += cmds[2];
			bind(gl, program.tTexture, textureAnimation(model.textures[cmds[0]]).texturenum);
			gl.drawArrays(gl.TRIANGLES, cmds[1], cmds[2]);
		}
	}
	program = useProgram(gl, 'Turbulent');
	gl.uniform3f(program.uOrigin, 0.0, 0.0, 0.0);
	gl.uniformMatrix3fv(program.uAngles, false, identity);
	gl.uniform1f(program.uTime, state.realtime % (Math.PI * 2.0));
	gl.vertexAttribPointer(program.aPosition.location, 3, gl.FLOAT, false, 20, model.waterchain);
	gl.vertexAttribPointer(program.aTexCoord.location, 2, gl.FLOAT, false, 20, model.waterchain + 12);
	for (i = 0; i < model.leafs.length; ++i)
	{
		leaf = model.leafs[i];
		if ((leaf.visframe !== state.visframecount) || (leaf.waterchain === leaf.cmds.length))
			continue;
		if (cullBox(leaf.mins, leaf.maxs) === true)
			continue;
		for (j = leaf.waterchain; j < leaf.cmds.length; ++j)
		{
			cmds = leaf.cmds[j];
			state.c_brush_verts += cmds[2];
			bind(gl, program.tTexture, model.textures[cmds[0]].texturenum);
			gl.drawArrays(gl.TRIANGLES, cmds[1], cmds[2]);
		}
	}
	unbindProgram(gl)
}

const updateTime = () => {
	state.realtime = Date.now() * 0.001 - state.oldtime;
}

export const draw = (gl: WebGLRenderingContext) => {
	updateTime()
	gl.clear(gl.COLOR_BUFFER_BIT + gl.DEPTH_BUFFER_BIT);
	calcRefdef()
	//animateLight();
	angleVectors(state.refdef.viewangles, state.vpn, state.vright, state.vup);
	
	state.viewleaf = pointInLeaf(state.refdef.vieworg, model);
	state.c_brush_verts = 0;
	state.c_alias_polys = 0;
	// v.setContentsColor(state.viewleaf.contents);
	// v.calcBlend();
	// state.dowarp = (cvr.waterwarp.value !== 0) && (state.viewleaf.contents <= mod.CONTENTS.water);
	setFov(gl)
	setFrustum()
	setupGL(gl);
	markLeaves();
	gl.enable(gl.CULL_FACE);
  gl.cullFace(gl.FRONT);
	drawSkyBox(gl);
	drawWorld(gl);
	gl.disable(gl.CULL_FACE);
}