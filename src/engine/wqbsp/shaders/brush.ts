export const vertex =
`uniform vec3 uOrigin;
uniform mat3 uAngles;
uniform vec3 uViewOrigin;
uniform mat3 uViewAngles;
uniform mat4 uPerspective;
attribute vec3 aPosition;
attribute vec4 aTexCoord;
attribute vec4 aLightStyle;
varying vec4 vTexCoord;
varying vec4 vLightStyle;
void main(void)
{
  vec3 position = uViewAngles * (uAngles * aPosition + uOrigin - uViewOrigin);
  gl_Position = uPerspective * vec4(position.xz, -position.y, 1.0);
  vTexCoord = aTexCoord;
  vLightStyle = aLightStyle;
}`

export const fragment =
`precision mediump float;
uniform float uGamma;
uniform sampler2D tTexture;
uniform sampler2D tLightmap;
varying vec4 vTexCoord;
void main(void)
{
  vec4 texture = texture2D(tTexture, vTexCoord.xy);
  gl_FragColor = vec4(texture.rgb *
    mix(1.0, dot(texture2D(tLightmap, vTexCoord.zw), vec4(
      .2,
      .2,
      .2,
      .2)
    * 43.828125), texture.a), 1.0);
  gl_FragColor.r = pow(gl_FragColor.r, uGamma);
  gl_FragColor.g = pow(gl_FragColor.g, uGamma);
  gl_FragColor.b = pow(gl_FragColor.b, uGamma);
}`