import * as brush from './shaders/brush'
import * as sky from './shaders/sky'
import * as skyChain from './shaders/skyChain'
import * as turbulent from './shaders/turbulent'

export const programs = []

var currentProgram = null

const loadProgram = (gl: WebGLRenderingContext,
  shader,
  identifier,
  uniforms, 
  attribs,
  textures) =>
{
  var p = gl.createProgram();
  var program =
  {
    identifier: identifier,
    program: p,
    attribs: []
  } as any;

  var vsh = gl.createShader(gl.VERTEX_SHADER);
  gl.shaderSource(vsh, shader.vertex);
  gl.compileShader(vsh);
  if (gl.getShaderParameter(vsh, gl.COMPILE_STATUS) !== true)
    throw new Error('Error compiling shader: ' + gl.getShaderInfoLog(vsh));

  var fsh = gl.createShader(gl.FRAGMENT_SHADER);
  gl.shaderSource(fsh, shader.fragment);
  gl.compileShader(fsh);
  if (gl.getShaderParameter(fsh, gl.COMPILE_STATUS) !== true)
    throw new Error('Error compiling shader: ' + gl.getShaderInfoLog(fsh));

  gl.attachShader(p, vsh);
  gl.attachShader(p, fsh);

  gl.linkProgram(p);
  if (gl.getProgramParameter(p, gl.LINK_STATUS) !== true)
    throw new Error('Error linking program: ' + gl.getProgramInfoLog(p));

  gl.useProgram(p);

  for (var i = 0; i < uniforms.length; ++i)
    program[uniforms[i]] = gl.getUniformLocation(p, uniforms[i]);

  program.vertexSize = 0;
  program.attribBits = 0;
  for (var i = 0; i < attribs.length; ++i)
  {
    var attribParameters = attribs[i];
    var attrib =
    {
      name: attribParameters[0],
      location: gl.getAttribLocation(p, attribParameters[0]),
      type: attribParameters[1],
      components: attribParameters[2],
      normalized: (attribParameters[3] === true),
      offset: program.vertexSize
    };
    program.attribs[i] = attrib;
    program[attrib.name] = attrib;
    if (attrib.type === gl.FLOAT)
      program.vertexSize += attrib.components * 4;
    else if (attrib.type === gl.BYTE || attrib.type === gl.UNSIGNED_BYTE)
      program.vertexSize += 4;
    else
      throw new Error('Unknown vertex attribute type');
    program.attribBits |= 1 << attrib.location;
  }

  for (var i = 0; i < textures.length; ++i)
  {
    program[textures[i]] = i;
    gl.uniform1i(gl.getUniformLocation(p, textures[i]), i);
  }

  programs[programs.length] = program;
  return program;
}

export const useProgram = (gl: WebGLRenderingContext, identifier: string) => {
  var _currentProgram = currentProgram;
  if (currentProgram != null)
  {
    if (_currentProgram.identifier === identifier)
      return _currentProgram;
  }

  var program = null;
  for (var i = 0; i < programs.length; ++i)
  {
    if (programs[i].identifier === identifier)
    {
      program = programs[i];
      break;
    }
  }
  if (program == null)
    return null;

  var enableAttribs = program.attribBits, disableAttribs = 0;
  if (currentProgram != null)
  {
    enableAttribs &= ~currentProgram.attribBits;
    disableAttribs = currentProgram.attribBits & ~program.attribBits;
  }
  currentProgram = program;
  gl.useProgram(program.program);
  for (var attrib = 0; enableAttribs !== 0 || disableAttribs !== 0; ++attrib)
  {
    var mask = 1 << attrib;
    if ((enableAttribs & mask) !== 0)
      gl.enableVertexAttribArray(attrib);
    else if ((disableAttribs & mask) !== 0)
      gl.disableVertexAttribArray(attrib);
    enableAttribs &= ~mask;
    disableAttribs &= ~mask;
  }

  return program;
}

export const unbindProgram = (gl) => {
  if (currentProgram == null)
    return;
  var i;
  for (i = 0; i < currentProgram.attribs.length; ++i)
    gl.disableVertexAttribArray(currentProgram.attribs[i].location);
  currentProgram = null;
};


const loadPrograms = (gl: WebGLRenderingContext) => {
	loadProgram(gl, brush, 'Brush',
    ['uOrigin', 'uAngles', 'uViewOrigin', 'uViewAngles', 'uPerspective', 'uGamma'],
    [['aPosition', gl.FLOAT, 3], ['aTexCoord', gl.FLOAT, 4], ['aLightStyle', gl.FLOAT, 4]],
    ['tTexture', 'tLightmap'])

  loadProgram(gl, sky, 'Sky',
    ['uViewAngles', 'uPerspective', 'uScale', 'uGamma', 'uTime'],
    [['aPosition', gl.FLOAT, 3]],
    ['tSolid', 'tAlpha']);

  loadProgram(gl, skyChain, 'SkyChain',
    ['uViewOrigin', 'uViewAngles', 'uPerspective'],
    [['aPosition', gl.FLOAT, 3]],
    []);

  loadProgram(gl, turbulent,'Turbulent',
    ['uOrigin', 'uAngles', 'uViewOrigin', 'uViewAngles', 'uPerspective', 'uGamma', 'uTime'],
    [['aPosition', gl.FLOAT, 3], ['aTexCoord', gl.FLOAT, 2]],
    ['tTexture']);
}

export const init = (gl: WebGLRenderingContext) => {
  loadPrograms(gl)
}