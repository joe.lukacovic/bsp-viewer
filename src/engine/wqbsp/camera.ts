import {dom} from '../glstate'
export const state = {
  origin: [0,0,0],
  angles: [0,0,0]
}

const keydown = (e) => {
  switch(e.code) {
    case 'ArrowUp':
      state.origin[0] += 1;
      break;
    case 'ArrowDown':
      state.origin[0] -= 1;
      break;
    case 'Space':
      state.origin[2] += 1;
      break;
    case 'c':
      state.origin[2] -= 1;
      break;
    case 'ArrowLeft':
      state.angles[1] += 1;
      break;
    case 'ArrowRight':
      state.angles[1] -= 1;
      break;
  }
}

export const setOrigin = (origin) => {
  state.origin = origin
}
export const setAngles = (angles) => {
  state.angles = angles
}

export const init = () => {
  dom.addEventHandler('keydown', keydown)
}