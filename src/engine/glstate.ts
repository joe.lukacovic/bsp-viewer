import * as WebGLDebugUtils from './debug.js'

export let gl = null
export let dom = null

const onError = (err,fnName, args) => {
  console.log('GL ERROR ' + fnName)
}

export const init = (_dom) => {
  dom = _dom
  const context = dom.canvas.getContext('webgl2') || dom.canvas.getContext('experimental-webgl2')
  //gl = WebGLDebugUtils.default.makeDebugContext(context, onError, null, null)
  gl = context
}