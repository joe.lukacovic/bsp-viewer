// export interface IModel {
// 	char		name[MAX_QPATH];
// 	unsigned int	path_id;		// path id of the game directory
// 							// that this model came from
// 	qboolean	needload;		// bmodels and sprites don't cache normally

// 	modtype_t	type;
// 	int			numframes;
// 	synctype_t	synctype;

// 	int			flags;

// //
// // volume occupied by the model graphics
// //
// 	vec3_t		mins, maxs;
// 	vec3_t		ymins, ymaxs; //johnfitz -- bounds for entities with nonzero yaw
// 	vec3_t		rmins, rmaxs; //johnfitz -- bounds for entities with nonzero pitch or roll
// 	//johnfitz -- removed float radius;

// //
// // solid volume for clipping
// //
// 	qboolean	clipbox;
// 	vec3_t		clipmins, clipmaxs;

// //
// // brush model
// //
// 	int			firstmodelsurface, nummodelsurfaces;

// 	int			numsubmodels;
// 	dmodel_t	*submodels;

// 	int			numplanes;
// 	mplane_t	*planes;

// 	int			numleafs;		// number of visible leafs, not counting 0
// 	mleaf_t		*leafs;

// 	int			numvertexes;
// 	mvertex_t	*vertexes;

// 	int			numedges;
// 	medge_t		*edges;

// 	int			numnodes;
// 	mnode_t		*nodes;

// 	int			numtexinfo;
// 	mtexinfo_t	*texinfo;

// 	int			numsurfaces;
// 	msurface_t	*surfaces;

// 	int			numsurfedges;
// 	int			*surfedges;

// 	int			numclipnodes;
// 	mclipnode_t	*clipnodes; //johnfitz -- was dclipnode_t

// 	int			nummarksurfaces;
// 	msurface_t	**marksurfaces;

// 	hull_t		hulls[MAX_MAP_HULLS];

// 	int			numtextures;
// 	texture_t	**textures;

// 	byte		*visdata;
// 	byte		*lightdata;
// 	char		*entities;

// 	qboolean	viswarn; // for Mod_DecompressVis()

// 	int			bspversion;
// }