export interface ISurface {
  visframe: number
  culled: boolean
  mins: Array<number>
  maxs: Array<number>
  flags: number
  firstedge: number
  texturemins: Array< number> 
  extents: Array<number>
  polys: Array<Array<Object>>
  texturechain: Array<Object>
  
}
// 	int			visframe;		// should be drawn when node is crossed
// 	qboolean	culled;			// johnfitz -- for frustum culling
// 	float		mins[3];		// johnfitz -- for frustum culling
// 	float		maxs[3];		// johnfitz -- for frustum culling

// 	mplane_t	*plane;
// 	int			flags;

// 	int			firstedge;	// look up in model->surfedges[], negative numbers
// 	int			numedges;	// are backwards edges

// 	short		texturemins[2];
// 	short		extents[2];

// 	int			light_s, light_t;	// gl lightmap coordinates

// 	glpoly_t	*polys;				// multiple if warped
// 	struct	msurface_s	*texturechain;

// 	mtexinfo_t	*texinfo;

// 	int		vbo_firstvert;		// index of this surface's first vert in the VBO

// // lighting info
// 	int			dlightframe;
// 	unsigned int		dlightbits[(MAX_DLIGHTS + 31) >> 5];
// 		// int is 32 bits, need an array for MAX_DLIGHTS > 32

// 	int			lightmaptexturenum;
// 	byte		styles[MAXLIGHTMAPS];
// 	int			cached_light[MAXLIGHTMAPS];	// values currently used in lightmap
// 	qboolean	cached_dlight;				// true if dynamic l