import {strmem} from './mem'

export const loadBinary = (url: string) : Promise<ArrayBuffer> => 
  new Promise((resolve, reject) => {
    const xhr = new XMLHttpRequest()
    xhr.overrideMimeType('text\/plain; charset=x-user-defined')
    xhr.open('GET', url)
    xhr.onload = () => {
      resolve(
        strmem(xhr.responseText)
      )
    }
    xhr.onerror = (e) => reject(e) 
    xhr.send();
  })


export const loadImage = (url) => {
  return new Promise((resolve, reject) => {
    const img = new Image()
  
    img.onload = () => resolve(img)
    img.onerror = e => reject(e)
    img.src = url;
  })
}