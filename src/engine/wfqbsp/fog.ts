import * as parse from './parse'
import * as common from './common'

const c = {
    parseState: parse.state
};

const DEFAULT_DENSITY = 0.0;
const DEFAULT_GRAY = 0.3;

const clamp = (min, v, max) => {
    return v < min ? min : v > max ? max : v;
}

export const state = {
    density: DEFAULT_DENSITY,
    color: {
        red: DEFAULT_GRAY,
        green: DEFAULT_GRAY,
        blue: DEFAULT_GRAY
    },
    transition: {
        density: 0,
        color: {
            red: 0,
            green: 0,
            blue: 0
        }
    },
    fade_time: 0,
    fade_done: 0
};
/*
=============
Fog_ParseWorldspawn

called at map load
=============
*/
export const init = (worldModel) => {
  state.density = DEFAULT_DENSITY;
  state.color.red = DEFAULT_GRAY;
  state.color.green = DEFAULT_GRAY;
  state.color.blue = DEFAULT_GRAY;
  var key, value, data;
  data = parse.parse(worldModel.entities);
  if (!data)
      return; // error
  if (parse.state.token[0] !== '{')
      return; // error
  while (1) {
    data = parse.parse(data);
    if (!data)
        return; // error
    if (c.parseState.token[0] === '}')
        break; // end of worldspawn
    if (c.parseState.token[0] === '_')
        key = parse.state.token.substr(1);
    else
        key = parse.state.token;
    key = key.trim();
    data = parse.parse(data);
    if (!data)
        return; // error
    value = parse.state.token;
    if (key === 'fog') {
      const split = value.split(' ');
      if (split.length === 4) {
        state.density = common.atof(split[0]);
        state.color.red = common.atof(split[1]);
        state.color.green = common.atof(split[2]);
        state.color.blue = common.atof(split[3]);
      }
      else {
        console.log(`Map has invalid fog value ${value}`);
      }
    }
  }
}
