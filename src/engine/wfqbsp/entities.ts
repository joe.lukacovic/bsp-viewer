import {parseEntities} from './entitiesParser'
export var entities = []

export const init = (model) => {
  entities = parseEntities(model.entities)
}