import {angleVectors, rotatePointAroundVector, dotProduct, boxOnPlaneSide, copy as vecCopy} from './vector'
import {world, pointInLeaf, leafPVS as bspLeafPVS} from './bsp'
import {createProgram, useProgram, unbindProgram, state as prgState, freePrograms} from './program'
import {CONTENTS, identity} from './common'
import {bind, freeTextures} from './textures'
import {dom} from '../glstate'
import {state as txState} from './textures'
import {entities} from './entities'
import {init as initCamera, setOrigin, setAngles, state as cameraState} from './camera'
import * as defs from './defs'
import * as worldShader from './shaders/world'
import * as turbulent from './shaders/turbulent'
import * as sky from './shaders/sky'
import * as skyChain from './shaders/skyChain'
import * as mod from './model'
import {init as initBatchRender, batchSurface, clearBatch, flushBatch} from './batchRender'
import {noVisPVS, leafPVS} from './model'
import * as mapAlpha from './mapAlpha'
import {
	init as lmInit, 
	state as lmState,
	buildLightmaps,
	LM_BLOCK_WIDTH, LM_BLOCK_HEIGHT
} from './lightmap'
import { ViewerOptions } from '../main'

const cvr = {
	gl_overbright: {value: 0},
	gl_fullbrights: {value: 0},
	r_novis: {value: 0},
	oldskyleaf: {value: 0}
}
const ATTR_INDEX = {
	vert: 0,
	texCoords: 1,
	LMCoords: 2
}
const state = {
	oldtime: 0.0,
	realtime: 0.0,
	viewEnt: {
		origin:  [0.0, 0.0, 0.0],
		angles:  [0.0, 0.0, 0.0]
	},
	visframecount: 0,
	frustum: [{} as any, {}as any, {}as any, {} as any],
	vup: [],
	vpn: [],
	vright: [],
	fov: 110,
	origin: [0.0, 0.0, 0.0],
	refdef: {
		vrect: {},
		vieworg: [0.0, 0.0, 0.0],
		viewangles: [0.0, 0.0, 0.0],
		fov_x: 0,
		fov_y: 0
	},
	c_brush_verts: 0,
	c_alias_polys: 0,
	drawsky: false,
	viewleaf: {} as any,
	oldviewleaf: null as any,
	perspective: [
		0.0, 0.0, 0.0, 0.0,
		0.0, 0.0, 0.0, 0.0,
		0.0, 0.0, -65540.0 / 65532.0, -1.0,
		0.0, 0.0, -524288.0 / 65532.0, 0.0
	],
	// settings
	gamma: 1,
	skyvecs: null,
	model_vbo: null,
	framecount: 0,
	last_lightmap_allocated: 0,
	vis_changed: false,
	cl_worldmodel: null,
	numVised: 0
}

export const init = (gl: WebGLRenderingContext, viewerOptions: ViewerOptions) => {
	state.cl_worldmodel = world
	state.oldtime = Date.now() * 0.001;

	state.viewEnt = entities.find(ent => ent.classname === 'info_player_start')
	state.viewEnt.origin[0] += 100
	// Aristacle
	//state.viewEnt.origin = [639.1316489123276, -1860.9381180926352, 43]
	// bloodshot fence
	// state.viewEnt.origin = [-176.3139242216108, 848.5747457043946, 54]
	// state.viewEnt.angles = [0, 205, 0]

	// e1m6 blank texture
	// state.viewEnt.origin = [-916.1644276287493, 435.0361505074511, 54.03125]
	// state.viewEnt.angles = [0, 195, 0]

	// // one thousand cuts surface issue
	// state.viewEnt.origin = [1682.5283760875907, 4.2139712770521305, 624.03125];
	// state.viewEnt.angles = [0, 720, 0]
	// e1m1 start loc
	state.viewEnt.origin = [480.03125, -351.96875, 98.03125];

	state.viewEnt.origin = [-13.559413794584694, 768.5360841995986, 98.0625]
	
  if (viewerOptions.origin) {
    state.viewEnt.origin = viewerOptions.origin
  }
	state.visframecount = 0;

	state.frustum = [{normal: []}, {normal: []}, {normal: []}, {normal: []}];

	state.vup = [];
	state.vpn = [];
	state.vright = [];
	state.c_brush_verts = 0;
	state.c_alias_polys = 0;
	state.fov =  110;
	state.refdef = {
		vrect: {},
		vieworg: [0.0, 0.0, 0.0],
		viewangles: [0.0, 0.0, 0.0],
		fov_x: 0,
		fov_y: 0
	}
	state.viewleaf = 0
	state.gamma = 1
	state.perspective = [
		0.0, 0.0, 0.0, 0.0,
		0.0, 0.0, 0.0, 0.0,
		0.0, 0.0, -65540.0 / 65532.0, -1.0,
		0.0, 0.0, -524288.0 / 65532.0, 0.0
	];

	createShaders(gl)
	
	// const lm_allocated = [];
	// for (var i = 0; i < LIGHTMAP_DIM; ++i)
	// 	lm_allocated[i] = 0;
	// for (var j = 0; j < model.faces.length; ++j)
	// {
	// 	var surf = model.faces[j];
	// 	if ((surf.sky !== true) && (surf.turbulent !== true))
	// 	{
	// 		allocBlock(lm_allocated, surf);
	// 		if (model.lightdata != null)
	// 			buildLightMap(model, surf);
	// 	}
	// 	buildSurfaceDisplayList(model, surf);
	// }
	lmInit()
	buildLightmaps(gl, state.cl_worldmodel)
	buildSurfaceDisplayLists (state.cl_worldmodel);
	buildModelVertexBuffer(gl, state.cl_worldmodel)
	initBatchRender(gl)
	// bind(gl, 0, txState.lightmap_texture);
	// gl.texImage2D(gl.TEXTURE_2D, 0, gl.RGBA, LIGHTMAP_DIM, LIGHTMAP_DIM, 0, gl.RGBA, gl.UNSIGNED_BYTE, state.lightmaps);
	initCamera()
	setOrigin(state.viewEnt.origin)
	setAngles(state.viewEnt.angles)
	makeSky(gl)
}

const createShaders = (gl: WebGLRenderingContext) => {
	// const glProg = createProgram(gl, 'world', world, [
	// 	{ name: 'Vert', index: ATTR_INDEX.vert },
	// 	{ name: 'TexCoords', index: ATTR_INDEX.texCoords },
	// 	{ name: 'LMCoords', index: ATTR_INDEX.LMCoords },	
	// ], 'world')
	createProgram(
		gl, worldShader, 'World', 
		['uUseFullbrightTex','uUseOverbright','uUseAlphaTest',
		'uAlpha','uPerspective', 'uViewAngles', 'uViewOrigin', 
		'uOrigin', 'uAngles', 'uFogDensity', 'uFogColor'],
		[
			['Vert', gl.FLOAT, 3, false, ATTR_INDEX.vert],
			['TexCoords', gl.FLOAT, 2, false, ATTR_INDEX.texCoords],
			['LMCoords', gl.FLOAT, 2, false, ATTR_INDEX.LMCoords],
		],
		['Tex', 'LMTex', 'FullbrightTex'])
		
	createProgram(gl, turbulent, 'Turbulent',
    ['uOrigin', 'uAngles', 'uViewOrigin', 'uViewAngles', 'uPerspective', 'uGamma', 'uTime', 'uAlpha'],
    [['aPosition', gl.FLOAT, 3], ['aTexCoord', gl.FLOAT, 2]],
    ['tTexture']);
}

const perspective = (gl: WebGLRenderingContext) => {
	var viewangles = [
		state.refdef.viewangles[0] * Math.PI / 180.0,
		(state.refdef.viewangles[1] - 90.0) * Math.PI / -180.0,
		state.refdef.viewangles[2] * Math.PI / -180.0
	];

	var sp = Math.sin(viewangles[0]);
	var cp = Math.cos(viewangles[0]);
	var sy = Math.sin(viewangles[1]);
	var cy = Math.cos(viewangles[1]);
	var sr = Math.sin(viewangles[2]);
	var cr = Math.cos(viewangles[2]);

	  
  // var f = 1.0 / Math.tan(fieldOfViewInRadians / 2);
  // var rangeInv = 1 / (near - far);
	// const 
	// const mdnPers =
	// [
  //   f / aspectRatio, 0,                          0,   0,
  //   0,               f,                          0,   0,
  //   0,               0,    (near + far) * rangeInv,  -1,
  //   0,               0,  near * far * rangeInv * 2,   0
  // ];
	// if (v.cvr.gamma.value < 0.5)
	// 	cvar.setValue('gamma', 0.5);
	// else if (v.cvr.gamma.value > 1.0)
	// 	cvar.setValue('gamma', 1.0);

	const viewMatrix = [
			cr * cy + sr * sp * sy,		cp * sy,	-sr * cy + cr * sp * sy,
			cr * -sy + sr * sp * cy,	cp * cy,	-sr * -sy + cr * sp * cy,
			sr * cp,					-sp,		cr * cp
		]
	const what = 4.0
	var ymax = what * Math.tan(state.refdef.fov_y * Math.PI / 360.0);
	state.perspective[0] = what / (ymax * dom.canvas.width / dom.canvas.height);
	state.perspective[5] = what / ymax;
	const programs = Object.values(prgState.programs)
	for (var i = 0; i < programs.length; ++i)
	{
		var program = programs[i];
		gl.useProgram(program.glHandle);
		if (program.uViewOrigin != null)
			gl.uniform3fv(program.uViewOrigin, state.refdef.vieworg);
		if (program.uViewAngles != null)
			gl.uniformMatrix3fv(program.uViewAngles, false, viewMatrix);
		if (program.uPerspective != null)
			gl.uniformMatrix4fv(program.uPerspective, false, state.perspective);
		if (program.uGamma != null)
			gl.uniform1f(program.uGamma, state.gamma);
	}
}


/*
================
BuildSurfaceDisplayLists -- called at level load time
================
*/
const buildSurfaceDisplayLists = (model) => {
	//
	// draw texture
	//
	for ( var i = 0; i < model.numfaces; i++) {
		
		if ((model.faces[i].flags & defs.SURF.drawtiled) && !(model.faces[i].flags & defs.SURF.drawtub))
			continue;
		var fa = model.faces[i]
		fa.polys = {
			next: fa.polys,
			numverts: fa.numedges,
			verts: []
		}

		const texInfo = model.texinfo[fa.texinfo]
		const texture = model.textures[texInfo.texture]

		for (var j = 0 ; j < fa.numedges; j++)
		{
			var lindex = model.surfedges[fa.firstedge + j];
			var _vec, s, t
			if (lindex > 0)
			{
				_vec = model.vertexes[model.edges[lindex][0]];
			}
			else
			{
				_vec = model.vertexes[model.edges[-lindex][1]];
			}
			s = dotProduct (_vec, texInfo.vecs[0]) + texInfo.vecs[0][3];
			s /= texture.width;

			t = dotProduct (_vec, texInfo.vecs[1]) + texInfo.vecs[1][3];
			t /= texture.height;

			fa.polys.verts[j] = []

			vecCopy (_vec, fa.polys.verts[j]);
			fa.polys.verts[j][3] = s;
			fa.polys.verts[j][4] = t;

			//
			// lightmap texture coordinates
			//
			s = dotProduct (_vec, texInfo.vecs[0]) + texInfo.vecs[0][3];
			s -= fa.texturemins[0];
			s += fa.light_s*16;
			s += 8;
			s /= LM_BLOCK_WIDTH*16; //fa->texinfo->texture->width;

			t = dotProduct (_vec, texInfo.vecs[1]) + texInfo.vecs[1][3];
			t -= fa.texturemins[1];
			t += fa.light_t*16;
			t += 8;
			t /= LM_BLOCK_HEIGHT*16; //fa->texinfo->texture->height;

			fa.polys.verts[j][5] = s;
			fa.polys.verts[j][6] = t;
		}

		//johnfitz -- removed gl_keeptjunctions code
	}
}


const buildModelVertexBuffer = (gl : WebGLRenderingContext, model) => {
	var v_buffer = []
	var v_index = 0
	
	for (var i = 0; i < model.faces.length; i++)  {
		const surf = model.faces[i]
		surf.vbo_firstvert = v_index
		for (var j = 0; j < surf.polys.verts.length; j++) 
			for (var k = 0; k < 7; k++)
				v_buffer.push(surf.polys.verts[j][k] || 0)

		v_index += surf.polys.verts.length
	}

	state.model_vbo = gl.createBuffer();
	gl.bindBuffer(gl.ARRAY_BUFFER, state.model_vbo);
	gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(v_buffer), gl.STATIC_DRAW);
}

const setFov = (gl) => {
	if ((dom.canvas.width * 0.75) <= dom.canvas.height)
	{
		state.refdef.fov_x = state.fov;
		state.refdef.fov_y = Math.atan(dom.canvas.height / (dom.canvas.width / Math.tan(state.fov * Math.PI / 360.0))) * 360.0 / Math.PI;
	}
	else
	{
		state.refdef.fov_x = Math.atan(dom.canvas.width / (dom.canvas.height / Math.tan(state.fov * 0.82 * Math.PI / 360.0))) * 360.0 / Math.PI;
		state.refdef.fov_y = state.fov * 0.82;
	}
}
const setFrustum = () => {
	state.frustum[0].normal = rotatePointAroundVector(state.vup, state.vpn, -(90.0 - state.refdef.fov_x * 0.5));
	state.frustum[1].normal = rotatePointAroundVector(state.vup, state.vpn, 90.0 - state.refdef.fov_x * 0.5);
	state.frustum[2].normal = rotatePointAroundVector(state.vright, state.vpn, 90.0 - state.refdef.fov_y * 0.5);
	state.frustum[3].normal = rotatePointAroundVector(state.vright, state.vpn, -(90.0 - state.refdef.fov_y * 0.5));
	var i, out;
	for (i = 0; i <= 3; ++i)
	{
		out = state.frustum[i];
		out.type = 5;
		out.dist = dotProduct(state.refdef.vieworg, out.normal);
		out.signbits = 0;
		if (out.normal[0] < 0.0)
			out.signbits = 1;
		if (out.normal[1] < 0.0)
			out.signbits += 2;
		if (out.normal[2] < 0.0)
			out.signbits += 4;
		if (out.normal[3] < 0.0)
			out.signbits += 8;
	}
}


/*
=============================================================================

The PVS must include a small area around the client to allow head bobbing
or other small motion on the client side.  Otherwise, a bob might cause an
entity that should be visible to not show up, especially when the bob
crosses a waterline.

=============================================================================
*/

var	fatbytes;
var fatpvs;
var fatpvs_capacity;
const addToFatPVS = (org, node, worldmodel) => { // johnfitz -- added worldmodel as a parameter

	while (1)
	{
		// if this is a leaf, accumulate the pvs bits
		if (node.contents < 0)
		{
			if (node.contents !== CONTENTS.solid)
			{
				var pvs = leafPVS (node, worldmodel); //johnfitz -- worldmodel as a parameter
				for (var i = 0 ; i < fatbytes ; i++)
					fatpvs[i] |= pvs[i];
			}
			return; 
		}

		var plane = node.plane;
		var d = dotProduct (org, plane.normal) - plane.dist;
		if (d > 8)
			node = node.children[0];
		else if (d < -8)
			node = node.children[1];
		else
		{	// go down both
			addToFatPVS (org, node.children[0], worldmodel); //johnfitz -- worldmodel as a parameter
			node = node.children[1];
		}
	}
}

/*
=============
SV_FatPVS

Calculates a PVS that is the inclusive or of all leafs within 8 pixels of the
given point.
=============
*/
const fatPVS = (org, worldmodel) => //johnfitz -- added worldmodel as a parameter
{
	fatbytes = (worldmodel.numleafs+7)>>3; // ericw -- was +31, assumed to be a bug/typo
	if (fatpvs == null || fatbytes > fatpvs_capacity)
	{
		fatpvs_capacity = fatbytes;
		fatpvs = new Uint8Array(new ArrayBuffer(fatpvs_capacity)).fill(0)
	}
	addToFatPVS (org, worldmodel.nodes, worldmodel); //johnfitz -- worldmodel as a parameter
	return fatpvs;
}

/*
================
R_ChainSurface -- ericw -- adds the given surface to its texture chain
================
*/
const chainSurface = (model, surf, chain) => {
	const texture = model.textures[model.texinfo[surf.texinfo].texture]
	surf.texturechain = texture.texturechains[chain];
	texture.texturechains[chain] = surf;
}


/*
===============
R_MarkSurfaces -- johnfitz -- mark surfaces based on PVS and rebuild texture chains
===============
*/
const markSurfaces = () => {
	var vis = []

	// // clear lightmap chains
	// state.lightmap_polys = Array.apply(null, new Array(MAXLIGHTMAPS)).map(() => {})

	// check this leaf for water portals
	// TODO: loop through all water surfs and use distance to leaf cullbox
	var nearwaterportal = false;
	for (var i = 0, mark = state.viewleaf.firstmarksurface; i <  state.viewleaf.nummarksurfaces; i++, mark++)
		if (mark.flags & defs.SURF.drawtub)
			nearwaterportal = true;

	// choose vis data
	if (cvr.r_novis.value || state.viewleaf.contents === CONTENTS.solid || state.viewleaf.contents === CONTENTS.sky)
		vis = noVisPVS (state.cl_worldmodel);
	else if (nearwaterportal)
		vis = fatPVS (state.origin, state.cl_worldmodel);
	else
		vis = leafPVS (state.viewleaf, state.cl_worldmodel);

	// if surface chains don't need regenerating, just add static entities and return
	if (state.oldviewleaf == state.viewleaf && !state.vis_changed && !nearwaterportal)
	{
		// TODO: efrags
		// var leaf = state.cl_worldmodel.leafs[1];
		// for (i = 0 ; i < state.cl_worldmodel.leafs.length ; i++, leaf++)
		// 	if (vis[i>>3] & (1<<(i&7)))
		// 		if (leaf.efrags)
		// 			R_StoreEfrags (&leaf->efrags);
		return;
	}

	state.vis_changed = false
	state.visframecount++;
	state.oldviewleaf = state.viewleaf
	state.numVised = 0

	var numSkyLeafs = 0
	// iterate through leaves, marking surfaces
	for (i=0; i < state.cl_worldmodel.numleafs; i++)
	{
		var leaf = state.cl_worldmodel.leafs[i + 1];
		if (vis[i>>3] & (1<<(i&7)))
		{
				if (leaf.contents == CONTENTS.sky)
				 	numSkyLeafs++
				if (cvr.oldskyleaf.value || leaf.contents != CONTENTS.sky) {
					for (var j = 0; j < leaf.nummarksurfaces; j++) {
						const surf = state.cl_worldmodel.faces[state.cl_worldmodel.marksurfaces[leaf.firstmarksurface + j]]
						surf.visframe = state.visframecount;
						state.numVised++
					}
				}

			// add static models // TODO: efrags
			// if (leaf->efrags)
			// 	R_StoreEfrags (&leaf->efrags);
		}
	}
	console.log(numSkyLeafs)
	
	// set all chains to null
	for (i=0 ; i<state.cl_worldmodel.textures.length ; i++)
		if (state.cl_worldmodel.textures[i])
			state.cl_worldmodel.textures[i].texturechains[defs.TEX_CHAIN.world] = null;

	// rebuild chains
	//iterate through surfaces one node at a time to rebuild chains
	//need to do it this way if we want to work with tyrann's skip removal tool
	//becuase his tool doesn't actually remove the surfaces from the bsp surfaces lump
	//nor does it remove references to them in each leaf's marksurfaces list
	for (i=0; i<state.cl_worldmodel.nodes.length ; i++)
		for (j=0; j<state.cl_worldmodel.nodes[i].numfaces ; j++) {
			var surf = state.cl_worldmodel.faces[state.cl_worldmodel.nodes[i].firstface + j]
			if (surf.visframe === state.visframecount) {
				chainSurface(state.cl_worldmodel, surf, defs.TEX_CHAIN.world);
			}
		}
	state.drawsky = true
}

export const recursiveWorldNode = function (node) {
	if (node.contents === mod.CONTENTS.solid)
		return;
	if (node.contents < 0) {
		if (node.markvisframe !== state.visframecount)
			return;
		node.visframe = state.visframecount;
		if (node.skychain !== node.waterchain)
			state.drawsky = true;
		return;
	}
	recursiveWorldNode(node.children[0]);
	recursiveWorldNode(node.children[1]);
};

export const markLeaves = function () {
	if ((state.oldviewleaf === state.viewleaf))
		return;
	++state.visframecount;
	state.oldviewleaf = state.viewleaf;
	var vis = mod.leafPVS(state.viewleaf, state.cl_worldmodel);
	var i, node;
	for (i = 0; i < state.cl_worldmodel.leafs.length; ++i) {
		if ((vis[i >> 3] & (1 << (i & 7))) === 0)
			continue;
		for (node = state.cl_worldmodel.leafs[i + 1]; node != null; node = node.parent) {
			if (node.markvisframe === state.visframecount)
				break;
			node.markvisframe = state.visframecount;
		}
	}
	do {
		var p = [state.refdef.vieworg[0], state.refdef.vieworg[1], state.refdef.vieworg[2]];
		var leaf;
		if (state.viewleaf.contents <= mod.CONTENTS.water) {
			leaf = mod.pointInLeaf([state.refdef.vieworg[0], state.refdef.vieworg[1], state.refdef.vieworg[2] + 16.0], state.cl_worldmodel);
			if (leaf.contents <= mod.CONTENTS.water)
				break;
		}
		else {
			leaf = mod.pointInLeaf([state.refdef.vieworg[0], state.refdef.vieworg[1], state.refdef.vieworg[2] - 16.0], state.cl_worldmodel);
			if (leaf.contents > mod.CONTENTS.water)
				break;
		}
		if (leaf === state.viewleaf)
			break;
		vis = mod.leafPVS(leaf, state.cl_worldmodel);
		for (i = 0; i < state.cl_worldmodel.leafs.length; ++i) {
			if ((vis[i >> 3] & (1 << (i & 7))) === 0)
				continue;
			for (node = state.cl_worldmodel.leafs[i + 1]; node != null; node = node.parent) {
				if (node.markvisframe === state.visframecount)
					break;
				node.markvisframe = state.visframecount;
			}
		}
	} while (false);
	state.drawsky = false;
	recursiveWorldNode(state.cl_worldmodel.nodes[0]);
};

// const buildLightmapChains = (model, chain) => {
// 	// texture_t *t;
// 	// msurface_t *s;

// 	// clear lightmap chains (already done in r_marksurfaces, but clearing them here to be safe becuase of r_stereo)
// 	state.lightmap_polys = []

// 	// now rebuild them
// 	for (var i = 0 ; i < model.textures.length; i++)
// 	{
// 		var tex = model.textures[i];

// 		if (!tex || !tex.texturechains[chain])
// 			continue;

// 		for (var s = tex.texturechains[chain]; !!s; s = s.texturechain)
// 			if (!s.culled)
// 				renderDynamicLightmaps (model, s);
// 	}
// }

const setupGL = (gl) =>
{
	gl.disable(gl.BLEND);
	gl.enable(gl.DEPTH_TEST);
};

export const calcRefdef = () => {

	state.refdef.vieworg[0] = cameraState.origin[0] + 0.03125;
	state.refdef.vieworg[1] = cameraState.origin[1] + 0.03125;
	state.refdef.vieworg[2] = cameraState.origin[2] + 0.03125;

	state.refdef.viewangles[0] = cameraState.angles[0]
	state.refdef.viewangles[1] = cameraState.angles[1]
	state.refdef.viewangles[2] = cameraState.angles[2]

	// var ipitch = cvr.idlescale.value * Math.sin(cl.clState.time * cvr.ipitch_cycle.value) * cvr.ipitch_level.value;
	// var iyaw = cvr.idlescale.value * Math.sin(cl.clState.time * cvr.iyaw_cycle.value) * cvr.iyaw_level.value;
	// var iroll = cvr.idlescale.value * Math.sin(cl.clState.time * cvr.iroll_cycle.value) * cvr.iroll_level.value;
	// r.state.refdef.viewangles[0] += ipitch;
	// r.state.refdef.viewangles[1] += iyaw;
	// r.state.refdef.viewangles[2] += iroll;

	// var forward = [], right = [], up = [];
	// vec.angleVectors([-ent.angles[0], ent.angles[1], ent.angles[2]], forward, right, up);
	// r.state.refdef.vieworg[0] += cvr.ofsx.value * forward[0] + cvr.ofsy.value * right[0] + cvr.ofsz.value * up[0];
	// r.state.refdef.vieworg[1] += cvr.ofsx.value * forward[1] + cvr.ofsy.value * right[1] + cvr.ofsz.value * up[1];
	// r.state.refdef.vieworg[2] += cvr.ofsx.value * forward[2] + cvr.ofsy.value * right[2] + cvr.ofsz.value * up[2];

	// if (r.state.refdef.vieworg[0] < (ent.origin[0] - 14.0))
	// 	r.state.refdef.vieworg[0] = ent.origin[0] - 14.0;
	// else if (r.state.refdef.vieworg[0] > (ent.origin[0] + 14.0))
	// 	r.state.refdef.vieworg[0] = ent.origin[0] + 14.0;
	// if (r.state.refdef.vieworg[1] < (ent.origin[1] - 14.0))
	// 	r.state.refdef.vieworg[1] = ent.origin[1] - 14.0;
	// else if (r.state.refdef.vieworg[1] > (ent.origin[1] + 14.0))
	// 	r.state.refdef.vieworg[1] = ent.origin[1] + 14.0;
	// if (r.state.refdef.vieworg[2] < (ent.origin[2] - 22.0))
	// 	r.state.refdef.vieworg[2] = ent.origin[2] - 22.0;
	// else if (r.state.refdef.vieworg[2] > (ent.origin[2] + 30.0))
	// 	r.state.refdef.vieworg[2] = ent.origin[2] + 30.0;

	// var view = cl.clState.viewent;
	// view.angles[0] = -r.state.refdef.viewangles[0] - ipitch;
	// view.angles[1] = r.state.refdef.viewangles[1] - iyaw;
	// view.angles[2] = cl.clState.viewangles[2] - iroll;
	// view.origin[0] = ent.origin[0] + forward[0] * bob * 0.4;
	// view.origin[1] = ent.origin[1] + forward[1] * bob * 0.4;
	// view.origin[2] = ent.origin[2] + cl.clState.viewheight + forward[2] * bob * 0.4 + bob;
	// switch (scr.cvr.viewsize.value)
	// {
	// case 110:
	// case 90:
	// 	view.origin[2] += 1.0;
	// 	break;
	// case 100:
	// 	view.origin[2] += 2.0;
	// 	break;
	// case 80:
	// 	view.origin[2] += 0.5;
	// }
	// view.model = cl.clState.model_precache[cl.clState.stats[def.STAT.weapon]];
	// view.frame = cl.clState.stats[def.STAT.weaponframe];

	// r.state.refdef.viewangles[0] += cl.clState.punchangle[0];
	// r.state.refdef.viewangles[1] += cl.clState.punchangle[1];
	// r.state.refdef.viewangles[2] += cl.clState.punchangle[2];

	// if ((cl.clState.onground === true) && ((ent.origin[2] - oldz) > 0.0))
	// {
	// 	var steptime = cl.clState.time - cl.clState.oldtime;
	// 	if (steptime < 0.0)
	// 		steptime = 0.0;
	// 	oldz += steptime * 80.0;
	// 	if (oldz > ent.origin[2])
	// 		oldz = ent.origin[2];
	// 	else if ((ent.origin[2] - oldz) > 12.0)
	// 		oldz = ent.origin[2] - 12.0;
	// 	r.state.refdef.vieworg[2] += oldz - ent.origin[2];
	// 	view.origin[2] += oldz - ent.origin[2];
	// }
	// else
	// 	oldz = ent.origin[2];
	// if (chase.cvr.active.value !== 0)
	// 	chase.update();
};


/*
================
R_BackFaceCull -- johnfitz -- returns true if the surface is facing away from vieworg
================
*/
const backFaceCull = (surf) => {
	var dot

	switch (surf.plane.type)
	{
	case defs.PLANE.x:
		dot = state.refdef.vieworg[0] - surf.plane.dist;
		break;
	case defs.PLANE.y:
		dot = state.refdef.vieworg[1] - surf.plane.dist;
		break;
	case defs.PLANE.z:
		dot = state.refdef.vieworg[2] - surf.plane.dist;
		break;
	default:
		dot = dotProduct (state.refdef.vieworg, surf.plane.normal) - surf.plane.dist;
		break;
	}

	if ((dot < 0) !== !!(surf.flags & defs.SURF.planeback))
		return true;

	return false;
}

/*
=================
R_CullBox -- johnfitz -- replaced with new function from lordhavoc

Returns true if the box is completely outside the frustum
=================
*/
const cullBox = (emins, emaxs) => {
	for (var i = 0; i < 4; i++)
	{
		var p = state.frustum[i];
		switch(p.signbits)
		{
			default:
			case 0:
				if (p.normal[0]*emaxs[0] + p.normal[1]*emaxs[1] + p.normal[2]*emaxs[2] < p.dist)
					return true;
				break;
			case 1:
				if (p.normal[0]*emins[0] + p.normal[1]*emaxs[1] + p.normal[2]*emaxs[2] < p.dist)
					return true;
				break;
			case 2:
				if (p.normal[0]*emaxs[0] + p.normal[1]*emins[1] + p.normal[2]*emaxs[2] < p.dist)
					return true;
				break;
			case 3:
				if (p.normal[0]*emins[0] + p.normal[1]*emins[1] + p.normal[2]*emaxs[2] < p.dist)
					return true;
				break;
			case 4:
				if (p.normal[0]*emaxs[0] + p.normal[1]*emaxs[1] + p.normal[2]*emins[2] < p.dist)
					return true;
				break;
			case 5:
				if (p.normal[0]*emins[0] + p.normal[1]*emaxs[1] + p.normal[2]*emins[2] < p.dist)
					return true;
				break;
			case 6:
				if (p.normal[0]*emaxs[0] + p.normal[1]*emins[1] + p.normal[2]*emins[2] < p.dist)
					return true;
				break;
			case 7:
				if (p.normal[0]*emins[0] + p.normal[1]*emins[1] + p.normal[2]*emins[2] < p.dist)
					return true;
				break;
		}
	}
	return false;
}

export const textureAnimation = function(model, base, entFrame)
{
	if (entFrame)
		if (base.alternate_anims)
			base = base.alternate_anims;

	var frame = 0;
	if (base.anim_base != null)
	{
		frame = base.anim_frame;
		base = model.textures[base.anim_base];
	}
	var anims = base.anims;
	if (anims == null)
		return base;
	return model.textures[anims[(Math.floor(state.realtime * 5.0) + frame) % anims.length]];
}

// export const animateLight = function(gl: WebGLRenderingContext)
// {
// 	var j;
// 	// if (cvr.fullbright.value === 0)
// 	// {
// 	// 	var i = Math.floor(cl.clState.time * 10.0);
// 	// 	for (j = 0; j < 64; ++j)
// 	// 	{
// 	// 		if (cl.state.lightstyle[j].length === 0)
// 	// 		{
// 	// 			state.lightstylevalue[j] = 12;
// 	// 			continue;
// 	// 		}
// 	// 		state.lightstylevalue[j] = cl.state.lightstyle[j].charCodeAt(i % cl.state.lightstyle[j].length) - 97;
// 	// 	}
// 	// }
// 	// else
// 	// {
// 		for (j = 0; j < 256; ++j)
// 			state.lightstylevalue[j] = 12; // Should this be 256?
// 	// }
// 	bind(gl, 0, txState.lightstyle_texture);
// 	gl.texImage2D(gl.TEXTURE_2D, 0, gl.ALPHA, 64, 1, 0, gl.ALPHA, gl.UNSIGNED_BYTE, state.lightstylevalue);
// };

/*
================
R_CullSurfaces -- johnfitz
================
*/
const cullSurfaces = (model, chain) => {

// ericw -- instead of testing (s->visframe == r_visframecount) on all world
// surfaces, use the chained surfaces, which is exactly the same set of sufaces
	for (var i=0 ; i<model.textures.length ; i++)
	{
		var t = model.textures[i];

		if (!t || !t.texturechains[chain])
			continue;

		for (var s = t.texturechains[chain]; s; s = s.texturechain)
		{
			if (cullBox(s.mins, s.maxs) && backFaceCull (s))
				s.culled = true;
			else
			{
				s.culled = false;
				// rs_brushpolys++; //count wpolys here // TODO stats
				const texture = model.textures[model.texinfo[s.texinfo].texture]
				if (texture.warpimage)
					texture.update_warp = true;
			}
		}
	}
}

// const renderDynamicLightmaps = (model, surf) =>
// {
// 	// byte		*base;
// 	// int			maps;
// 	// glRect_t    *theRect;
// 	// int smax, tmax;

// 	if (surf.flags & defs.SURF.drawtiled) //johnfitz -- not a lightmapped surface
// 		return;

// 	// add to lightmap chain
// 	surf.polys.chain = state.lightmap_polys[surf.lightmaptexturenum];
// 	state.lightmap_polys[surf.lightmaptexturenum] = surf.polys;
// 	var doDynamic = false
// 	// check for lightmap modification
// 	for (var maps=0; maps < MAXLIGHTMAPS && surf.styles[maps]; maps++)
// 		if (state.lightstylevalue[surf.styles[maps]] != surf.cached_light[maps]){
// 			doDynamic= true
// 			break
// 		}

// 	if (doDynamic 
// 		|| surf.dlightframe == state.framecount	// dynamic this frame
// 		|| surf.cached_dlight)			// dynamic previously
// 	{
// 		if (true) // (r_dynamic.value)
// 		{
// 			state.lightmap_modified[surf.lightmaptexturenum] = true;
// 			var theRect = state.lightmap_rectchange[surf.lightmaptexturenum];
// 			if (surf.light_t < theRect.t) {
// 				if (theRect.h)
// 					theRect.h += theRect.t - surf.light_t;
// 				theRect.t = surf.light_t;
// 			}
// 			if (surf.light_s < theRect.l) {
// 				if (theRect.w)
// 					theRect.w += theRect.l - surf.light_s;
// 				theRect.l = surf.light_s;
// 			}
// 			var smax = (surf.extents[0]>>4)+1;
// 			var tmax = (surf.extents[1]>>4)+1;
// 			if ((theRect.w + theRect.l) < (surf.light_s + smax))
// 				theRect.w = (surf.light_s-theRect.l)+smax;
// 			if ((theRect.h + theRect.t) < (surf.light_t + tmax))
// 				theRect.h = (surf.light_t-theRect.t)+tmax;
// 			var bufOfs = surf.lightmaptexturenum * state.lightmap_bytes * LM_BLOCK_WIDTH * LM_BLOCK_HEIGHT;
// 			bufOfs += surf.light_t * LM_BLOCK_WIDTH * state.lightmap_bytes + surf.light_s * state.lightmap_bytes;
// 			buildLightMap (model, surf, bufOfs, LM_BLOCK_WIDTH * state.lightmap_bytes);
// 		}
// 	}
// }

const waterAlphaForSurface = (surf) => {
	if (surf.flags & defs.SURF.drawlava)
		return mapAlpha.state.lava > 0 ? mapAlpha.state.lava : mapAlpha.state.water;
	else if (surf.flags & defs.SURF.drawtele)
		return mapAlpha.state.tele > 0 ? mapAlpha.state.tele : mapAlpha.state.water;
	else if (surf.flags & defs.SURF.drawslime)
		return mapAlpha.state.slime > 0 ? mapAlpha.state.slime : mapAlpha.state.water;
	else
		return mapAlpha.state.water;
}


/*
================
GL_WaterAlphaForEntitySurface -- ericw
 
Returns the water alpha to use for the entity and surface combination.
================
*/
const waterAlphaForEntitySurface = (ent, surf) => {
	var entalpha = 1
	if (!ent || ent.alpha == 1)
		entalpha = waterAlphaForSurface(surf);
	else
		entalpha = 1 // pr.decodeAlpha(ent.alpha);
	return entalpha;
}
/*
================
R_DrawTextureChains_Water -- johnfitz
================
*/
const drawWater = (gl: WebGLRenderingContext, model, ent, chain) => {
	
	const turbulentProgram = useProgram(gl, 'Turbulent')

	// Bind the buffers
	gl.bindBuffer (gl.ARRAY_BUFFER, state.model_vbo);
	gl.bindBuffer (gl.ELEMENT_ARRAY_BUFFER, null)// indices come from client memory!
	
	gl.vertexAttribPointer (ATTR_INDEX.vert,      3, gl.FLOAT, false, defs.VERTEXSIZE * 4, 0);
	gl.vertexAttribPointer (ATTR_INDEX.texCoords, 2, gl.FLOAT, false, defs.VERTEXSIZE * 4, 4 * 3);

	// set uniforms
	gl.uniform1i (turbulentProgram.uUseOverbright, cvr.gl_overbright.value);
	gl.uniform1i (turbulentProgram.uUseAlphaTest, 0);

	gl.uniform3f(turbulentProgram.uOrigin, 0.0, 0.0, 0.0);
	gl.uniformMatrix3fv(turbulentProgram.uAngles, false, identity);
	gl.uniform1f(turbulentProgram.uTime, state.realtime % (Math.PI * 2.0))
	gl.uniform1f(turbulentProgram.uAlpha, .5);

	for (var i=0 ; i<model.textures.length ; i++)
	{
		var t = model.textures[i];
		if (!t || !t.texturechains[chain] || !(t.texturechains[chain].flags & defs.SURF.drawtub))
			continue;
		var animatedTexture = textureAnimation(state.cl_worldmodel, t, ent != null ? ent.frame : 0)
		clearBatch ();
		var bound = false;
		var entalpha = .5;
		for (var s = t.texturechains[chain]; s; s = s.texturechain)
			if (!s.culled)
			{
				if (!bound) //only bind once we are sure we need this texture
				{
					bind (gl, 0, animatedTexture.texturenum);
					bound = true;
				}
				var	newalpha = waterAlphaForEntitySurface (ent, s);
				if (newalpha !== entalpha) {
					if (newalpha < 1)
					{
						gl.depthMask(false);
						gl.enable(gl.BLEND);
						gl.blendFunc(gl.SRC_ALPHA, gl.ONE_MINUS_SRC_ALPHA);
					} else {
						gl.depthMask(true);
						gl.disable(gl.BLEND);
					}
					gl.uniform1f(turbulentProgram.uAlpha, newalpha);
				}
				entalpha = newalpha

				batchSurface (gl, s);
			}
		
		//R_EndTransparentDrawing (entalpha);
		flushBatch(gl)

		if (entalpha < 1)
		{
			gl.depthMask(true);
			gl.disable(gl.BLEND);
		}
	}


	unbindProgram(gl)
}

/*
================
R_DrawTextureChains -- ericw

Draw lightmapped surfaces with fulbrights in one pass, using VBO.
Requires 3 TMUs, OpenGL 2.0
================
*/
const drawTextureChains = (gl, model, ent, chain) => {
	var entalpha = 1
	
	// if (ent != NULL)
	// 	entalpha = ENTALPHA_DECODE(ent->alpha);
	// else
	// 	entalpha = 1;

	// TODO Dynamic LMs
// ericw -- the mh dynamic lightmap speedup: make a first pass through all
// surfaces we are going to draw, and rebuild any lightmaps that need it.
// this also chains surfaces by lightmap which is used by r_lightmap 1.
// the previous implementation of the speedup uploaded lightmaps one frame
// late which was visible under some conditions, this method avoids that.
	//buildLightmapChains (model, chain);
	//uploadLightmaps (gl);

	var	fullbright = null
	
	// enable blending / disable depth writes
	if (entalpha < 1)
	{
		gl.depthMask (gl.FALSE);
		gl.enable (gl.BLEND);
	}

	const worldProgram = useProgram(gl, 'World')
	
	// Bind the buffers
	gl.bindBuffer (gl.ARRAY_BUFFER, state.model_vbo);
	gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, null)// indices come from client memory!

	// Until all shaders use "useProgram" we do this manually 
	gl.enableVertexAttribArray (ATTR_INDEX.vert);
	gl.enableVertexAttribArray (ATTR_INDEX.texCoords);
	gl.enableVertexAttribArray (ATTR_INDEX.LMCoords);
	
	gl.vertexAttribPointer (ATTR_INDEX.vert,      3, gl.FLOAT, gl.FALSE, defs.VERTEXSIZE * 4, 0);
	gl.vertexAttribPointer (ATTR_INDEX.texCoords, 2, gl.FLOAT, gl.FALSE, defs.VERTEXSIZE * 4, 4 * 3);
	gl.vertexAttribPointer (ATTR_INDEX.LMCoords,  2, gl.FLOAT, gl.FALSE, defs.VERTEXSIZE * 4, 4 * 5);
	
	// set uniforms
	// gl.uniform1i (uniforms.tex, 0);
	// gl.uniform1i (uniforms.LMTex, 1);
	// gl.uniform1i (uniforms.fullbrightTex, 2);
	gl.uniform1i (worldProgram.uUseFullbrightTex, 0);
	gl.uniform1i (worldProgram.uUseOverbright, 1);
	gl.uniform1i (worldProgram.uUseAlphaTest, 0);
	gl.uniform1f (worldProgram.uAlpha, entalpha);
	gl.uniform1f(worldProgram.uFogDensity, 0.0 / 64)
	gl.uniform4f (worldProgram.uFogColor, .2, .16, .15, 1)

	// gl.uniform1i (worldProgram.tex, 0)
	// gl.uniform1i (worldProgram.LMTex, 1)
	// //gl.uniform1i (uniforms.fullbrightTex, 2)

	gl.uniform3f(worldProgram.uOrigin, 0.0, 0.0, 0.0);
	gl.uniformMatrix3fv(worldProgram.uAngles, false, identity);
	var i = 0;
	for (var i = 0; i < model.textures.length; i++)
	{
		var t = model.textures[i];

		if (!t || !t.texturechains[chain] || t.texturechains[chain].flags & (defs.SURF.drawtiled | defs.SURF.notexture | defs.SURF.drawtub))
			continue;

		var animatedTexture = textureAnimation(state.cl_worldmodel, t, ent != null ? ent.frame : 0)
	// Enable/disable TMU 2 (fullbrights)
	// FIXME: Move below to where we bind GL_TEXTURE0
		if (cvr.gl_fullbrights.value && (fullbright = animatedTexture.fullbright))
		{
			//gl.activeTexture (gl.TEXTURE2);
			bind (gl, 2, fullbright);
			gl.uniform1i(worldProgram.uUseFullbrightTex, 1);
		}
		else {
			gl.uniform1i(worldProgram.uUseFullbrightTex, 0);
			bind(gl, 2, txState.null_texture)
		}

		clearBatch ();

		var bound = false;
		var lastlightmap = 0; // avoid compiler warning
		for (var s = t.texturechains[chain]; !!s; s = s.texturechain)
			if (!s.culled)
			{
				if (!bound) //only bind once we are sure we need this texture
				{
					// gl.activeTexture(gl.TEXTURE0);
					bind (gl, 0, animatedTexture.texturenum);
					
					if (t.texturechains[chain].flags & defs.SURF.drawfence)
						gl.uniform1i(worldProgram.uUseAlphaTest, 1); // Flip alpha test back on
										
					bound = true;
					lastlightmap = s.lightmaptexturenum;
				}
				
				if (s.lightmaptexturenum !== lastlightmap)
					flushBatch(gl);

				//gl.activeTexture (gl.TEXTURE1);
				bind (gl, 1, txState.lightmap_textures[s.lightmaptexturenum].texnum);
				// gl.activeTexture(gl.TEXTURE1)
				// gl.bindTexture(gl.TEXTURE_2D, txState.lightmap_textures[s.lightmaptexturenum].texnum);
				lastlightmap = s.lightmaptexturenum;
				batchSurface (gl, s);

				// rs_brushpasses++; // stats
			}

		flushBatch(gl);

		if (bound && t.texturechains[chain].flags & defs.SURF.drawfence)
			gl.uniform1i (worldProgram.useAlphaTest, 0); // Flip alpha test back off
	}
	
	// clean up
	gl.disableVertexAttribArray(ATTR_INDEX.vert);
	gl.disableVertexAttribArray(ATTR_INDEX.texCoords);
	gl.disableVertexAttribArray(ATTR_INDEX.LMCoords);
	
	unbindProgram(gl)
	//gl.selectTexture(gl.TEXTURE0);
	
	if (entalpha < 1)
	{
		gl.depthMask(gl.TRUE);
		gl.disable(gl.BLEND);
	}
}

const updateTime = () => {
	state.realtime = Date.now() * 0.001 - state.oldtime;
}

const drawWorld = (gl: WebGLRenderingContext) => {
	drawTextureChains (gl, world, null, defs.TEX_CHAIN.world);
}

export const makeSky = function (gl: WebGLRenderingContext) {
	var sin = [0.0, 0.19509, 0.382683, 0.55557, 0.707107, 0.831470, 0.92388, 0.980785, 1.0];
	var vecs = [], i, j;

	for (i = 0; i < 7; i += 2) {
		vecs = vecs.concat(
			[
				0.0, 0.0, 1.0,
				sin[i + 2] * 0.19509, sin[6 - i] * 0.19509, 0.980785,
				sin[i] * 0.19509, sin[8 - i] * 0.19509, 0.980785
			]);
		for (j = 0; j < 7; ++j) {
			vecs = vecs.concat(
				[
					sin[i] * sin[8 - j], sin[8 - i] * sin[8 - j], sin[j],
					sin[i] * sin[7 - j], sin[8 - i] * sin[7 - j], sin[j + 1],
					sin[i + 2] * sin[7 - j], sin[6 - i] * sin[7 - j], sin[j + 1],

					sin[i] * sin[8 - j], sin[8 - i] * sin[8 - j], sin[j],
					sin[i + 2] * sin[7 - j], sin[6 - i] * sin[7 - j], sin[j + 1],
					sin[i + 2] * sin[8 - j], sin[6 - i] * sin[8 - j], sin[j]
				]);
		}
	}

	createProgram(gl, sky, 'Sky',
		['uViewAngles', 'uPerspective', 'uScale', 'uGamma', 'uTime'],
		[['aPosition', gl.FLOAT, 3]],
		['tSolid', 'tAlpha']);
	createProgram(gl, skyChain,  'SkyChain',
		['uViewOrigin', 'uViewAngles', 'uPerspective'],
		[['aPosition', gl.FLOAT, 3]],
		[]);

	state.skyvecs = gl.createBuffer();
	gl.bindBuffer(gl.ARRAY_BUFFER, state.skyvecs);
	gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(vecs), gl.STATIC_DRAW);
};

export const drawSkyBox = function (gl: WebGLRenderingContext) {
	if (state.drawsky !== true)
		return;

	gl.colorMask(false, false, false, false);
	var clmodel = state.cl_worldmodel;
	var program = useProgram(gl, 'SkyChain');
	gl.bindBuffer(gl.ARRAY_BUFFER,  state.model_vbo);
	gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, null)
	gl.vertexAttribPointer(program.aPosition.location, 3, gl.FLOAT, false, defs.VERTEXSIZE * 4, 0);
	for (var i = 0; i < clmodel.textures.length; i++) {
		var t = clmodel.textures[i];
		if (!t || !t.texturechains || !t.texturechains[defs.TEX_CHAIN.world] || !(t.texturechains[defs.TEX_CHAIN.world].flags & defs.SURF.drawsky))
			continue;
		for (var s = t.texturechains[defs.TEX_CHAIN.world]; !!s; s = s.texturechain)
			if (!s.culled) {
				batchSurface(gl, s);
			}
	}
	flushBatch(gl);

	gl.colorMask(true, true, true, true);

	gl.depthFunc(gl.GREATER);
	gl.depthMask(false);
	gl.disable(gl.CULL_FACE);

	program = useProgram(gl, 'Sky');
	gl.uniform2f(program.uTime, (state.realtime * 0.125) % 1.0, (state.realtime * 0.03125) % 1.0);
	bind(gl, program.tSolid, txState.solidskytexture);
	bind(gl, program.tAlpha, txState.alphaskytexture);
	gl.bindBuffer(gl.ARRAY_BUFFER, state.skyvecs);
	gl.vertexAttribPointer(program.aPosition.location, 3, gl.FLOAT, false, 12, 0);

	gl.uniform3f(program.uScale, 2.0, -2.0, 1.0);
	gl.drawArrays(gl.TRIANGLES, 0, 180);
	gl.uniform3f(program.uScale, 2.0, -2.0, -1.0);
	gl.drawArrays(gl.TRIANGLES, 0, 180);

	gl.uniform3f(program.uScale, 2.0, 2.0, 1.0);
	gl.drawArrays(gl.TRIANGLES, 0, 180);
	gl.uniform3f(program.uScale, 2.0, 2.0, -1.0);
	gl.drawArrays(gl.TRIANGLES, 0, 180);

	gl.uniform3f(program.uScale, -2.0, -2.0, 1.0);
	gl.drawArrays(gl.TRIANGLES, 0, 180);
	gl.uniform3f(program.uScale, -2.0, -2.0, -1.0);
	gl.drawArrays(gl.TRIANGLES, 0, 180);

	gl.uniform3f(program.uScale, -2.0, 2.0, 1.0);
	gl.drawArrays(gl.TRIANGLES, 0, 180);
	gl.uniform3f(program.uScale, -2.0, 2.0, -1.0);
	gl.drawArrays(gl.TRIANGLES, 0, 180);

	gl.enable(gl.CULL_FACE);
	gl.depthMask(true);
	gl.depthFunc(gl.LESS);
};
// export const setupView = (gl: WebGLRenderingContext) => {
// }

export const draw = (gl: WebGLRenderingContext) => {
	updateTime()
	gl.clear(gl.COLOR_BUFFER_BIT + gl.DEPTH_BUFFER_BIT);
	calcRefdef()

	vecCopy(state.refdef.vieworg, state.origin)
	angleVectors(state.refdef.viewangles, state.vpn, state.vright, state.vup)
	state.oldviewleaf = state.viewleaf
	state.viewleaf = pointInLeaf(state.origin, state.cl_worldmodel)
	
	setFrustum()
	markSurfaces();
	cullSurfaces(state.cl_worldmodel, defs.TEX_CHAIN.world)

	state.c_brush_verts = 0;
	state.c_alias_polys = 0;
	// v.setContentsColor(state.viewleaf.contents);
	// v.calcBlend();
	// state.dowarp = (cvr.waterwarp.value !== 0) && (state.viewleaf.contents <= mod.CONTENTS.water);
	setFov(gl)
	setupGL(gl);
	gl.enable(gl.CULL_FACE);
  gl.cullFace(gl.FRONT);
	
	perspective(gl)
	drawSkyBox(gl);
	drawWorld(gl)
	drawWater(gl, state.cl_worldmodel, null, defs.TEX_CHAIN.world)
	gl.disable(gl.CULL_FACE);
}

export const free = (gl) => {
	freePrograms(gl);
	freeTextures(gl);
};