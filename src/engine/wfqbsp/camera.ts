import {dom} from '../glstate'
import {angleVectors, normalize} from './vector'

const bindings = {
  ArrowUp: 'FORWARD',
  ArrowDown: 'BACK',
  Space: 'MOVEUP',
  KeyC: 'MOVEDOWN',
  ArrowLeft: 'TURNLEFT',
  ArrowRight: 'TURNRIGHT'
};

export const state = {
  origin: [0, 0, 0],
  angles: [0, 0, 0],
  maxSpeed: 600,
  keyedState: {}
}

const move = (forwardMove, sideMove) => {
  var forward = [], right = [];
  angleVectors(state.angles, forward, right, null);
  var fmove = forwardMove;
  var smove = sideMove;
  var wishvel = [
    forward[0] * fmove + right[0] * smove,
    forward[1] * fmove + right[1] * smove,
    0.0
  ]
  var wishdir = [wishvel[0], wishvel[1], wishvel[2]], wishspeed = normalize(wishdir);
  var scaler = (state.maxSpeed / wishspeed);
  if (wishspeed > state.maxSpeed) {
    wishvel[0] = wishvel[0] * scaler;
    wishvel[1] = wishvel[1] * scaler;
    wishvel[2] = wishvel[2] * scaler;
    wishspeed = state.maxSpeed;
  }
  const newOrigin = [
    state.origin[0] + wishvel[0],
    state.origin[1] + wishvel[1],
    state.origin[2] + wishvel[2],
  ];
  state.origin = newOrigin;
};
const keydown = (e) => {
  console.log('keydown ' + e.code);
  if (bindings[e.code]) {
    state.keyedState[bindings[e.code]] = true;
  }
};
const keyUp = (e) => {
  console.log('keyup ' + e.code);
  if (state.keyedState[bindings[e.code]]) {
      state.keyedState[bindings[e.code]] = false;
  }
};
export const sampleInput = () => {
  const input = Object.keys(state.keyedState).filter(key => !!state.keyedState[key]);
  for (var i = 0; i < input.length; i++) {
      switch (input[i]) {
          case 'FORWARD':
              //state.origin[0] += 3;
              move(3, 0);
              break;
          case 'BACK':
              move(-3, 0);
              break;
          case 'MOVEUP':
              state.origin[2] += 3;
              break;
          case 'MOVEDOWN':
              state.origin[2] -= 3;
              break;
          case 'TURNLEFT':
              state.angles[1] += 3;
              break;
          case 'TURNRIGHT':
              state.angles[1] -= 3;
              break;
      }
  }
};
export const doWobble = (time) => {
  const _time = time / 5;
  state.angles[0] = Math.sin(_time) * 3;
  state.angles[1] = 45 + Math.sin(_time * 2) * 3;
  state.angles[2] = Math.sin(_time * .5) * 1;
};
export const setOrigin = (origin) => {
  state.origin = origin;
};
export const setAngles = (angles) => {
  state.angles = angles;
};
export const init = () => {
  dom.addEventHandler('keydown', keydown);
  dom.addEventHandler('keyup', keyUp);
};
