import * as parse from './parse'
import * as common from './common'
const c = {
	parseState: parse.state
};
export const state = {
  lava: 0,
  water: 1,
  slime: 0,
  tele: 0
}

export const init = (worldModel) => {
	
	var key, value, data;
	data = parse.parse(worldModel.entities);
	if (!data)
			return; // error
	if (parse.state.token[0] != '{')
			return; // error
	while (1) {
			data = parse.parse(data);
			if (!data)
					return; // error
			if (c.parseState.token[0] === '}')
					break; // end of worldspawn
			if (c.parseState.token[0] == '_')
					key = parse.state.token.substr(1);
			else
					key = parse.state.token;
			key = key.trim();
			data = parse.parse(data);
			if (!data)
					return; // error
			value = parse.state.token;
			if (key === "wateralpha")
					state.water = common.atof(value);
			if (key === "lavaalpha")
					state.lava = common.atof(value);
			if (key === "telealpha")
					state.tele = common.atof(value);
			if (key === "slimealpha")
					state.slime = common.atof(value);
	}
}
