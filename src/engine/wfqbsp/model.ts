import {bufferGrow} from './common'
export const state = {
  mod_novis: null,
	mod_novis_capacity: 0,
	mod_decompressed: null,
	mod_decompressed_capacity: 0
}

export const CONTENTS = {
  empty: -1,
  solid: -2,
  water: -3,
  slime: -4,
  lava: -5,
  sky: -6,
  origin: -7,
  clip: -8,
  current_0: -9,
  current_90: -10,
  current_180: -11,
  current_270: -12,
  current_up: -13,
  current_down: -14
};


export const pointInLeaf = function(p, model)
{
  if (model == null)
    throw new Error('Mod.PointInLeaf: bad model');
  if (model.nodes == null)
		throw new Error('Mod.PointInLeaf: bad model');
  var node = model.nodes[0];
  var normal;
  for (;;)
  {
    if (node.contents < 0)
      return node;
    normal = node.plane.normal;
    if ((p[0] * normal[0] + p[1] * normal[1] + p[2] * normal[2] - node.plane.dist) > 0)
      node = node.children[0];
    else
      node = node.children[1];
  }
};

export const noVisPVS = (model) => {
	const pvsbytes = (model.numleafs+7) >> 3;
	if (state.mod_novis === null || pvsbytes > state.mod_novis_capacity)
	{
		state.mod_novis_capacity = pvsbytes;
		state.mod_novis = new Uint8Array(new ArrayBuffer(state.mod_novis_capacity)) 
		state.mod_novis.fill(0xFF)
	}
	return state.mod_novis;
}

export const leafPVS = (leaf, model) => {
	if (leaf == model.leafs)
		return noVisPVS (model);
	return decompressVis (leaf.compressed_vis, model);
}

/*
===================
Mod_DecompressVis
===================
*/
const decompressVis = (visData, model) => {
	const row = (model.numleafs+7)>>3;
	if (state.mod_decompressed === null || row > state.mod_decompressed_capacity)
	{
		state.mod_decompressed_capacity = row;
		state.mod_decompressed = state.mod_decompressed
			? new Uint8Array(bufferGrow(state.mod_decompressed.buffer, row))
			: new Uint8Array(new ArrayBuffer(state.mod_decompressed_capacity))
	}

	if (!visData)
	{	
		// no vis info, keep all visible
		state.mod_decompressed.fill(0xFF)
		
		return state.mod_decompressed;
	}

	var visCounter = 0;
	var outCounter = 0;

	do
	{
		if (visData[visCounter])
		{
			state.mod_decompressed[outCounter++] = visData[visCounter++];
			continue;
		}

		var c = visData[visCounter + 1];
		visCounter += 2;
		while (c)
		{
			if (outCounter === row)
			{
				if(!model.viswarn) {
					model.viswarn = true;
					console.log("Mod_DecompressVis: output overrun on model \"%s\"\n", model.name);
				}
				return state.mod_decompressed;
			}
			state.mod_decompressed[outCounter++] = 0;
			c--;
		}
	} while (outCounter < row);

	return state.mod_decompressed;
}

/*
=================
Mod_CalcSurfaceBounds -- johnfitz -- calculate bounding box for per-surface frustum culling
=================
*/
export const calcSurfaceBounds = (model, surf) => {
	// int			i, e;
	// mvertex_t	*v;

	surf.mins[0] = surf.mins[1] = surf.mins[2] = 9999;
	surf.maxs[0] = surf.maxs[1] = surf.maxs[2] = -9999;

	for (var i = 0 ; i < surf.numedges ; i++)
	{
		var v, e = model.surfedges[surf.firstedge + i];
		if (e >= 0)
			v = model.vertexes[model.edges[e][0]];
		else
			v = model.vertexes[model.edges[-e][1]];

		if (surf.mins[0] > v[0])
			surf.mins[0] = v[0];
		if (surf.mins[1] > v[1])
			surf.mins[1] = v[1];
		if (surf.mins[2] > v[2])
			surf.mins[2] = v[2];

		if (surf.maxs[0] < v[0])
			surf.maxs[0] = v[0];
		if (surf.maxs[1] < v[1])
			surf.maxs[1] = v[1];
		if (surf.maxs[2] < v[2])
			surf.maxs[2] = v[2];
	}
}
