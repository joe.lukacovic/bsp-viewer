import {loadTexture, loadSky, state as txState} from './textures'
import {memstr} from '../helpers/mem'
import {dotProduct, length as vecLength, copy as vecCopy} from './vector'
import {littleLong, radiusFromBounds} from './common'
import {calcSurfaceBounds} from './model'
import * as defs from './defs'

const known = []

const EFFECTS = {
  brightfield: 1,
  muzzleflash: 2,
  brightlight: 4,
  dimlight: 8
};

const TYPE = {
  brush: 0,
  sprite: 1,
  alias: 2
};

const FLAGS = {
  rocket: 1,
  grenade: 2,
  gib: 4,
  rotate: 8,
  tracer: 16,
  zomgib: 32,
  tracer2: 64,
  tracer3: 128
};

const LUMP =
{
  entities: 0,
  planes: 1,
  textures: 2,
  vertexes: 3,
  visibility: 4,
  nodes: 5,
  texinfo: 6,
  faces: 7,
  lighting: 8,
  clipnodes: 9,
  leafs: 10,
  marksurfaces: 11,
  edges: 12,
  surfedges: 13,
  models: 14
};


const VERSION = {
  'bsp2': (('B'.charCodeAt(0) << 0)  | ('S'.charCodeAt(0) << 8)  | ('P'.charCodeAt(0) << 16) | ('2'.charCodeAt(0) << 24)),
  '2psb': (('B'.charCodeAt(0) << 24) | ('S'.charCodeAt(0) << 16) | ('P'.charCodeAt(0) << 8)  | '2'.charCodeAt(0)),
  brush: 29,
  sprite: 1,
  alias: 6
};

/*
================
Mod_PolyForUnlitSurface -- johnfitz -- creates polys for unlightmapped surfaces (sky and water)

TODO: merge this into BuildSurfaceDisplayList?
================
*/
const polyForUnlitSurface = (loadmodel, fa) => {
	// vec3_t		verts[64];
	// int			numverts, i, lindex;
	// float		*vec;
	// glpoly_t	*poly;
  var texscale, _vec;
  
	if (fa.flags & (defs.SURF.drawtub | defs.SURF.drawsky))
		texscale = (1.0/128.0); //warp animation repeats every 128
	else
		texscale = (1.0/32.0); //to match r_notexture_mip

  fa.polys = {
    next: null,
    numverts: fa.numedges,
    verts: []
  }
  const texinfo = loadmodel.texinfo[fa.texinfo]
	// convert edges back to a normal polygon
	for (var i = 0 ; i < fa.numedges; i++)
	{
		var lindex = loadmodel.surfedges[fa.firstedge + i];

		if (lindex > 0)
			_vec = loadmodel.vertexes[loadmodel.edges[lindex][0]];
		else
      _vec = loadmodel.vertexes[loadmodel.edges[-lindex][1]];

    fa.polys.verts[i] = []
    vecCopy (_vec, fa.polys.verts[i]);
    
		fa.polys.verts[i][3] = dotProduct(_vec, texinfo.vecs[0]) * texscale;
		fa.polys.verts[i][4] = dotProduct(_vec, texinfo.vecs[1]) * texscale;
  }
}

const findName = function(name)
{
  if (name.length === 0)
    throw new Error('Mod.FindName: NULL name');
  var i;
  for (i = 0; i < known.length; ++i)
  {
    if (known[i] == null)
      continue;
    if (known[i].name === name)
      return known[i];
  }
  for (i = 0; i <= known.length; ++i)
  {
    if (known[i] != null)
      continue;
    known[i] = {name: name, needload: true};
    return known[i];
  }
};

const loadVertexes = function(loadmodel, buf)
{
  var view = new DataView(buf);
  var fileofs = view.getUint32((LUMP.vertexes << 3) + 4, true);
  var filelen = view.getUint32((LUMP.vertexes << 3) + 8, true);
  if ((filelen % 12) !== 0)
    throw new Error('Mod.LoadVisibility: funny lump size in ' + loadmodel.name);
  var count = filelen / 12;
  loadmodel.vertexes = [];
  var i;
  for (i = 0; i < count; ++i)
  {
    loadmodel.vertexes[i] = [view.getFloat32(fileofs, true), view.getFloat32(fileofs + 4, true), view.getFloat32(fileofs + 8, true)];
    fileofs += 12;
  }
}

const loadEdges = function(loadmodel, buf, bspVersion)
{
  var view = new DataView(buf);
  var fileofs = view.getUint32((LUMP.edges << 3) + 4, true);
  var filelen = view.getUint32((LUMP.edges << 3) + 8, true);
  if ((filelen & 3) !== 0)
    throw new Error('Mod.LoadEdges: funny lump size in ' + loadmodel.name);
  var count = filelen >> 2;
  loadmodel.edges = [];
  var i;

  if (bspVersion === VERSION["2psb"] || bspVersion === VERSION['bsp2']) {
    for (i = 0; i < count; ++i)
    {
      loadmodel.edges[i] = [view.getUint32(fileofs, true), view.getUint32(fileofs + 4, true)];
      fileofs += 8;
    }
  } else {
    for (i = 0; i < count; ++i)
    {
      loadmodel.edges[i] = [view.getUint16(fileofs, true), view.getUint16(fileofs + 2, true)];
      fileofs += 4;
    }
  }
}

const loadSurfedges = function(loadmodel, buf)
{
  var view = new DataView(buf);
  var fileofs = view.getUint32((LUMP.surfedges << 3) + 4, true);
  var filelen = view.getUint32((LUMP.surfedges << 3) + 8, true);
  var count = filelen >> 2;
  loadmodel.surfedges = [];
  var i;
  for (i = 0; i < count; ++i)
    loadmodel.surfedges[i] = view.getInt32(fileofs + (i << 2), true);
}

const loadTextures = function(gl, loadmodel, buf)
{
  var view = new DataView(buf);
  var fileofs = view.getUint32((LUMP.textures << 3) + 4, true);
  var filelen = view.getUint32((LUMP.textures << 3) + 8, true);
  loadmodel.textures = [];
  var nummiptex = view.getUint32(fileofs, true);
  var dataofs = fileofs + 4;
  var i, miptexofs, tx, glt;
  for (i = 0; i < nummiptex; ++i)
  {
    miptexofs = view.getInt32(dataofs, true);
    dataofs += 4;
    if (miptexofs === -1)
    {
      loadmodel.textures[i] = txState.notexture_mip;
      continue;
    }
    miptexofs += fileofs;
    tx = {
      name: memstr(new Uint8Array(buf, miptexofs, 16)),
      width: view.getUint32(miptexofs + 16, true),
      height: view.getUint32(miptexofs + 20, true),
      texturechains: [null, null]
    }
    if (tx.name.substring(0, 3).toLowerCase() === 'sky')
    {
      loadSky(gl, new Uint8Array(buf, miptexofs + view.getUint32(miptexofs + 24, true), 32768));
      tx.texturenum = txState.solidskytexture;
 
      tx.sky = true;
    }
    else 
    {
      var flags = 0
      if (tx.name[0] == '*') {
        // turbulent
        tx.turbulent = true
        flags |= defs.TEXPREF.nopicmip
        flags |= defs.TEXPREF.warpimage
      }  else {
        flags |= defs.TEXPREF.mipmap
      }
      if (tx.name[0] ==='{') {
        // fence texture
        flags |= defs.TEXPREF.alpha
      }
      const txData = new Uint8Array(buf, miptexofs + view.getUint32(miptexofs + 24, true), tx.width * tx.height)
      if (txData.some(b => b > 223)) 
      {
        glt = loadTexture(
          gl,
          tx.name,
          tx.width, 
          tx.height, 
          txData,
          flags | defs.TEXPREF.nobright
        )
        const glFullbright = loadTexture(
          gl,
          tx.name,
          tx.width, 
          tx.height, 
          txData,
          flags | defs.TEXPREF.fullbright
        )
        tx.fullbrightnum = glFullbright.texnum
      }
      else
      {
        glt = loadTexture(
          gl,
          tx.name,
          tx.width, 
          tx.height, 
          txData,
          flags
        )
      }

      tx.texturenum = glt.texnum;
    }
    loadmodel.textures[i] = tx;
  }

  var j, tx2, num, name;
  for (i = 0; i < nummiptex; ++i)
  {
    tx = loadmodel.textures[i];
    if (tx.name.charCodeAt(0) !== 43)
      continue;
    if (tx.name.charCodeAt(1) !== 48)
      continue;
    name = tx.name.substring(2);
    tx.anims = [i];
    tx.alternate_anims = [];
    for (j = 0; j < nummiptex; ++j)
    {
      tx2 = loadmodel.textures[j];
      if (tx2.name.charCodeAt(0) !== 43)
        continue;
      if (tx2.name.substring(2) !== name)
        continue;
      num = tx2.name.charCodeAt(1);
      if (num === 48)
        continue;
      if ((num >= 49) && (num <= 57))
      {
        tx.anims[num - 48] = j;
        tx2.anim_base = i;
        tx2.anim_frame = num - 48;
        continue;
      }
      if (num >= 97)
        num -= 32;
      if ((num >= 65) && (num <= 74))
      {
        tx.alternate_anims[num - 65] = j;
        tx2.anim_base = i;
        tx2.anim_frame = num - 65;
        continue;
      }
      throw new Error('Bad animating texture ' + tx.name);
    }
    for (j = 0; j < tx.anims.length; ++j)
    {
      if (tx.anims[j] == null)
        throw new Error('Missing frame ' + j + ' of ' + tx.name);
    }
    for (j = 0; j < tx.alternate_anims.length; ++j)
    {
      if (tx.alternate_anims[j] == null)
        throw new Error('Missing frame ' + j + ' of ' + tx.name);
    }
    loadmodel.textures[i] = tx;
  }

  loadmodel.textures[loadmodel.textures.length] = txState.notexture_mip;
}

const loadLighting = (loadmodel, buf) => {
  var view = new DataView(buf);
  var fileofs = view.getUint32((LUMP.lighting << 3) + 4, true);
  var filelen = view.getUint32((LUMP.lighting << 3) + 8, true);
  if (filelen === 0)
    return;
  loadmodel.lightdata = new Uint8Array(new ArrayBuffer(filelen));
  loadmodel.lightdata.set(new Uint8Array(buf, fileofs, filelen));
}

const loadPlanes = (loadmodel, buf) => {
  var view = new DataView(buf);
  var fileofs = view.getUint32((LUMP.planes << 3) + 4, true);
  var filelen = view.getUint32((LUMP.planes << 3) + 8, true);
  if ((filelen % 20) !== 0)
    throw new Error('Mod.LoadPlanes: funny lump size in ' + loadmodel.name);
  var count = filelen / 20;
  loadmodel.planes = [];
  var i, out;
  for (i = 0; i < count; ++i)
  {
    out = {
      normal: [view.getFloat32(fileofs, true), view.getFloat32(fileofs + 4, true), view.getFloat32(fileofs + 8, true)],
      dist: view.getFloat32(fileofs + 12, true),
      type: view.getUint32(fileofs + 16, true),
      signbits: 0
    };
    if (out.normal[0] < 0)
      ++out.signbits;
    if (out.normal[1] < 0)
      out.signbits += 2;
    if (out.normal[2] < 0)
      out.signbits += 4;
    loadmodel.planes[i] = out;
    fileofs += 20;
  }
}


const loadTexinfo = (loadmodel, buf) => {
  var view = new DataView(buf);
  var fileofs = view.getUint32((LUMP.texinfo << 3) + 4, true);
  var filelen = view.getUint32((LUMP.texinfo << 3) + 8, true);
  if ((filelen % 40) !== 0)
    throw new Error('Mod.LoadTexinfo: funny lump size in ' + loadmodel.name);
  var count = filelen / 40;
  loadmodel.texinfo = [];
  var i, out;
  for (i = 0; i < count; ++i)
  {
    out = {
      vecs: [
        [view.getFloat32(fileofs, true), view.getFloat32(fileofs + 4, true), view.getFloat32(fileofs + 8, true), view.getFloat32(fileofs + 12, true)],
        [view.getFloat32(fileofs + 16, true), view.getFloat32(fileofs + 20, true), view.getFloat32(fileofs + 24, true), view.getFloat32(fileofs + 28, true)]
      ],
      texture: view.getUint32(fileofs + 32, true),
      flags: view.getUint32(fileofs + 36, true)
    };
    if (out.texture >= loadmodel.textures.length)
    {
      out.texture = loadmodel.textures.length - 1;
      out.flags = 0;
    }
    loadmodel.texinfo[i] = out;
    fileofs += 40;
  }
}

const loadFaces = (loadmodel, buf, bspVersion) =>
{
  var size = bspVersion === VERSION["2psb"] || bspVersion === VERSION['bsp2'] ? 28 : 20
  var view = new DataView(buf);
  var fileofs = view.getUint32((LUMP.faces << 3) + 4, true);
  var filelen = view.getUint32((LUMP.faces << 3) + 8, true);
  if ((filelen % size) !== 0)
    throw new Error('Mod.LoadFaces: funny lump size in ' + loadmodel.name);
  var count = filelen / size;
  loadmodel.firstface = 0;
  loadmodel.numfaces = count;
  loadmodel.faces = [];
  var i, styles, out;
  var mins, maxs, j, e, tex, v, val, side;
  for (i = 0; i < count; ++i)
  {
    if (bspVersion === VERSION["2psb"] || bspVersion === VERSION['bsp2']) {
      styles = new Uint8Array(buf, fileofs + 20, 4);
      out = {
        plane: loadmodel.planes[view.getUint32(fileofs, true)],
        side: view.getUint32(fileofs + 4, true),
        firstedge: view.getUint32(fileofs + 8, true),
        numedges: view.getUint32(fileofs + 12, true),
        texinfo: view.getUint32(fileofs + 16, true),
        styles: [],
        lightofs: view.getInt32(fileofs + 24, true),
        mins: [],
        maxs: []
      }
      fileofs += 28;
    } else {
      styles = new Uint8Array(buf, fileofs + 12, 4);
      out = {
        plane: loadmodel.planes[view.getUint16(fileofs, true)],
        side: view.getUint16(fileofs + 2, true),
        firstedge: view.getUint32(fileofs + 4, true),
        numedges: view.getUint16(fileofs + 8, true),
        texinfo: view.getUint16(fileofs + 10, true),
        styles: [],
        lightofs: view.getInt32(fileofs + 16, true),
        mins: [],
        maxs: []
      }
      fileofs += 20;
    }

    for (j = 0; j < 4; j++) {
      if (styles[j] !== 255)
        out.styles[j] = styles[j];
    }

    mins = [999999, 999999];
    maxs = [-99999, -99999];
    tex = loadmodel.texinfo[out.texinfo];
    out.texture = tex.texture;
		out.flags = 0;

		if (out.side) // side
      out.flags |= defs.SURF.planeback
    
    if (loadmodel.textures[tex.texture].sky){
      out.flags |= (defs.SURF.drawsky | defs.SURF.drawtiled)
      out.sky = true;
      polyForUnlitSurface(loadmodel, out);
    }
    else if (loadmodel.textures[tex.texture].turbulent) {
      out.flags |= (defs.SURF.drawtub | defs.SURF.drawtiled)
      out.turbulent = true; 
      
      // detect special liquid types
      
      if (loadmodel.textures[tex.texture].name.substring(0, 5).toLowerCase() === '*lava')
        out.flags |= defs.SURF.drawlava
      else if (loadmodel.textures[tex.texture].name.substring(0, 6).toLowerCase() === '*slime')
        out.flags |= defs.SURF.drawslime
      else if (loadmodel.textures[tex.texture].name.substring(0, 5).toLowerCase() === '*tele')
        out.flags |= defs.SURF.drawtele
      else out.flags |= defs.SURF.drawwater;
      
      polyForUnlitSurface(loadmodel, out);
      // GL_SubdivideSurface (out);
    } else if (loadmodel.textures[tex.texture].name[0] === '{') {
      out.flags |= defs.SURF.drawfence
    } else if (tex.flags & defs.TEX.missing) {
      if (out.lightofs < 0) {
        out.flags |= (defs.SURF.notexture | defs.SURF.drawtiled);
        polyForUnlitSurface(loadmodel, out);
      } else {
        out.flags |= defs.SURF.notexture
      }
    }

    for (j = 0; j < out.numedges; ++j)
    {
      e = loadmodel.surfedges[out.firstedge + j];
      if (e >= 0)
        v = loadmodel.vertexes[loadmodel.edges[e][0]];
      else
        v = loadmodel.vertexes[loadmodel.edges[-e][1]];
      val = dotProduct(v, tex.vecs[0]) + tex.vecs[0][3];
      if (val < mins[0])
        mins[0] = val;
      if (val > maxs[0])
        maxs[0] = val;
      val = dotProduct(v, tex.vecs[1]) + tex.vecs[1][3];
      if (val < mins[1])
        mins[1] = val;
      if (val > maxs[1])
        maxs[1] = val;
    }

		// calcSurfaceExtents (out);
    calcSurfaceBounds(loadmodel, out)
    
    out.texturemins = [Math.floor(mins[0] / 16) * 16, Math.floor(mins[1] / 16) * 16];
    out.extents = [Math.ceil(maxs[0] / 16) * 16 - out.texturemins[0], Math.ceil(maxs[1] / 16) * 16 - out.texturemins[1]];

    if (!(tex.flags & defs.TEX.special) && 
      (out.extents[0] > 2000 || out.extents[1] > 2000))
      throw new Error("Bad surface extents")

    out.cached_light = []
    loadmodel.faces[i] = out;
  }
}

const loadMarksurfaces = (loadmodel, buf, bspVersion) => {
  var size = bspVersion === VERSION["2psb"] || bspVersion === VERSION['bsp2'] ? 4 : 2
  var view = new DataView(buf);
  var fileofs = view.getUint32((LUMP.marksurfaces << 3) + 4, true);
  var filelen = view.getUint32((LUMP.marksurfaces << 3) + 8, true);
  var count = filelen / size;
  loadmodel.marksurfaces = [];
  var i, j;
  for (i = 0; i < count; ++i) {
    if (bspVersion === VERSION["2psb"] || bspVersion === VERSION['bsp2']) { 
      j = view.getUint32(fileofs + (i << 2), true);
    } else {
      j = view.getUint16(fileofs + (i << 1), true);
    }
    if (j > loadmodel.faces.length)
      throw new Error('Mod.LoadMarksurfaces: bad surface number');
    loadmodel.marksurfaces[i] = j;
  }
}

const loadVisibility = (loadmodel, buf) => {
  var view = new DataView(buf);
  var fileofs = view.getUint32((LUMP.visibility << 3) + 4, true);
  var filelen = view.getUint32((LUMP.visibility << 3) + 8, true);
  if (filelen === 0)
    return;
  loadmodel.visdata = new Uint8Array(new ArrayBuffer(filelen));
  loadmodel.visdata.set(new Uint8Array(buf, fileofs, filelen));
}


const loadLeafs = (loadmodel, buf, bspVersion) =>{
  var size = bspVersion === VERSION["2psb"] ? 32 :
    bspVersion === VERSION.bsp2 ? 44 : 28
  var view = new DataView(buf);
  var fileofs = view.getUint32((LUMP.leafs << 3) + 4, true);
  var filelen = view.getUint32((LUMP.leafs << 3) + 8, true);
  if ((filelen % size) !== 0)
    throw new Error('Mod.LoadLeafs: funny lump size in ' + loadmodel.name);
  var count = filelen / size;
  loadmodel.leafs = [];
  var i, j, out;
  for (i = 0; i < count; ++i)
  {
    switch (bspVersion) {
      case VERSION["2psb"]:
        out = {
          num: i,
          contents: view.getInt32(fileofs, true),
          visofs: view.getInt32(fileofs + 4, true),
          mins: [view.getInt16(fileofs + 8, true), view.getInt16(fileofs + 10, true), view.getInt16(fileofs + 12, true)],
          maxs: [view.getInt16(fileofs + 14, true), view.getInt16(fileofs + 16, true), view.getInt16(fileofs + 18, true)],
          firstmarksurface: view.getUint32(fileofs + 20, true),
          nummarksurfaces: view.getUint32(fileofs + 24, true),
          ambient_level: [view.getUint8(fileofs + 28), view.getUint8(fileofs + 29), view.getUint8(fileofs + 30), view.getUint8(fileofs + 31)],
          cmds: [],
          skychain: 0,
          waterchain: 0
        };
        loadmodel.leafs[i] = out
        fileofs += 32
        break
      case VERSION['bsp2']:
          out = {
            num: i,
            contents: view.getInt32(fileofs, true),
            visofs: view.getInt32(fileofs + 4, true),
            mins: [view.getFloat32(fileofs + 8, true), view.getFloat32(fileofs + 12, true), view.getFloat32(fileofs + 16, true)],
            maxs: [view.getFloat32(fileofs + 20, true), view.getFloat32(fileofs + 24, true), view.getFloat32(fileofs + 28, true)],
            firstmarksurface: view.getUint32(fileofs + 32, true),
            nummarksurfaces: view.getUint32(fileofs + 36, true),
            ambient_level: [view.getUint8(fileofs + 40), view.getUint8(fileofs + 41), view.getUint8(fileofs + 42), view.getUint8(fileofs + 43)],
            cmds: [],
            skychain: 0,
            waterchain: 0
          }
          loadmodel.leafs[i] = out
          fileofs += 44
        break
      default:
        out = {
          num: i,
          contents: view.getInt32(fileofs, true),
          visofs: view.getInt32(fileofs + 4, true),
          mins: [view.getInt16(fileofs + 8, true), view.getInt16(fileofs + 10, true), view.getInt16(fileofs + 12, true)],
          maxs: [view.getInt16(fileofs + 14, true), view.getInt16(fileofs + 16, true), view.getInt16(fileofs + 18, true)],
          firstmarksurface: view.getUint16(fileofs + 20, true),
          nummarksurfaces: view.getUint16(fileofs + 22, true),
          ambient_level: [view.getUint8(fileofs + 24), view.getUint8(fileofs + 25), view.getUint8(fileofs + 26), view.getUint8(fileofs + 27)],
          cmds: [],
          skychain: 0,
          waterchain: 0
        };
        loadmodel.leafs[i] = out
        fileofs += 28
      break
    }
    var p = littleLong(out.visofs);
		if (p === -1)
			out.compressed_vis = null;
		else
			out.compressed_vis = loadmodel.visdata.subarray(p)
    loadmodel.leafs[i] = out;
  }
}

const setParent = (node, parent) => {
  node.parent = parent;
  if (node.contents < 0)
    return;
  setParent(node.children[0], node);
  setParent(node.children[1], node);
}

const loadNodes = (loadmodel, buf, bspVersion) => {
  var size = bspVersion === VERSION["2psb"] ? 32 : 
    bspVersion === VERSION.bsp2 ? 44 : 24
  var view = new DataView(buf);
  var fileofs = view.getUint32((LUMP.nodes << 3) + 4, true);
  var filelen = view.getUint32((LUMP.nodes << 3) + 8, true);
  if ((filelen === 0) || ((filelen % size) !== 0))
    throw new Error('Mod.LoadNodes: funny lump size in ' + loadmodel.name);
  var count = filelen / size;
  loadmodel.nodes = [];
  var i, out;

  switch (bspVersion) {
    case VERSION["2psb"]:
      return loadNodes_2psb(loadmodel, view, count, fileofs)
    case VERSION['bsp2']:
      return loadNodes_bsp2(loadmodel, view, count, fileofs)
    default:
      return loadNodes_s(loadmodel, view, count, fileofs)
  }
}

const loadNodes_s = (loadmodel, view, count, fileofs) => {
  loadmodel.nodes = [];
  var i, out;
  
  for (i = 0; i < count; ++i) {
    loadmodel.nodes[i] = {
      num: i,
      contents: 0,
      planenum: view.getUint32(fileofs, true),
      children: [view.getInt16(fileofs + 4, true), view.getInt16(fileofs + 6, true)],
      mins: [view.getInt16(fileofs + 8, true), view.getInt16(fileofs + 10, true), view.getInt16(fileofs + 12, true)],
      maxs: [view.getInt16(fileofs + 14, true), view.getInt16(fileofs + 16, true), view.getInt16(fileofs + 18, true)],
      firstface: view.getUint16(fileofs + 20, true),
      numfaces: view.getUint16(fileofs + 22, true),
      cmds: []
    };
    fileofs += 24;
  }
  for (i = 0; i < count; ++i)
  {
    out = loadmodel.nodes[i];
    out.plane = loadmodel.planes[out.planenum];
    
    if (out.children[0] >= 0)
      out.children[0] = loadmodel.nodes[out.children[0]];
    else
      out.children[0] = loadmodel.leafs[-1 - out.children[0]];
    if (out.children[1] >= 0)
      out.children[1] = loadmodel.nodes[out.children[1]];
    else
      out.children[1] = loadmodel.leafs[-1 - out.children[1]];
  }
  setParent(loadmodel.nodes[0], undefined);
}
const loadNodes_2psb = (loadmodel, view, count, fileofs) => {
  loadmodel.nodes = [];
  var i,j, out, p
  
  for (i = 0; i < count; ++i) {
    loadmodel.nodes[i] = {
      num: i,
      contents: 0,
      planenum: view.getUint32(fileofs, true),
      children: [view.getInt32(fileofs + 4, true), view.getInt32(fileofs + 8, true)],
      mins: [view.getInt16(fileofs + 12, true), view.getInt16(fileofs + 14, true), view.getInt16(fileofs + 16, true)],
      maxs: [view.getInt16(fileofs + 18, true), view.getInt16(fileofs + 20, true), view.getInt16(fileofs + 22, true)],
      firstface: view.getUint32(fileofs + 24, true),
      numfaces: view.getUint32(fileofs + 28, true),
      cmds: []
    };
    fileofs += 32;
  }

  for (i = 0; i < count; ++i) {
    out = loadmodel.nodes[i];
    out.plane = loadmodel.planes[out.planenum];
    for (j = 0; j < 2; j++) {
      
      p = out.children[j]
      if (p >= 0 && p < count) {
        out.children[j] = loadmodel.nodes[p];
      } else {
        p = (new Uint32Array([0xffffffff - p]))[0];
        if ( p >= 0 && p < loadmodel.leafs.length) {
          out.children[j] = loadmodel.leafs[p]
        } else {
          console.log(`Mod_LoadNodes: invalid leaf index ${p} (file has only ${loadmodel.leafs.length} leafs)\n`)
          out.children[j] = loadmodel.leafs[0]
        }
      }
    }
  }
  setParent(loadmodel.nodes[0], undefined);
}

const loadNodes_bsp2 = (loadmodel, view, count, fileofs) => {

  loadmodel.nodes = [];
  var i,j, out, p
  
  for (i = 0; i < count; ++i) {
    loadmodel.nodes[i] = {
      num: i,
      contents: 0,
      planenum: view.getUint32(fileofs, true),
      children: [view.getInt32(fileofs + 4, true), view.getInt32(fileofs + 8, true)],
      mins: [view.getFloat32(fileofs + 12, true), view.getFloat32(fileofs + 16, true), view.getFloat32(fileofs + 20, true)],
      maxs: [view.getFloat32(fileofs + 24, true), view.getFloat32(fileofs + 28, true), view.getFloat32(fileofs + 32, true)],
      firstface: view.getUint32(fileofs + 36, true),
      numfaces: view.getUint32(fileofs + 40, true),
      cmds: []
    };
    fileofs += 44;
  }
  
  for (i = 0; i < count; ++i) {
    out = loadmodel.nodes[i];
    out.plane = loadmodel.planes[out.planenum];
    for (j = 0; j < 2; j++) {
			//johnfitz -- hack to handle nodes > 32k, adapted from darkplaces
      p = out.children[j]
      if (p > 0 && p < count) {
        out.children[j] = loadmodel.nodes[p];
      } else {
        p = (new Uint32Array([0xffffffff - p]))[0];
        if ( p >= 0 && p < loadmodel.leafs.length) {
          out.children[j] = loadmodel.leafs[p]
        } else {
          console.log(`Mod_LoadNodes: invalid leaf index ${p} (file has only ${loadmodel.leafs.length} leafs)\n`)
          out.children[j] = loadmodel.leafs[0]
        }
      }
    }
  }
  setParent(loadmodel.nodes[0], undefined);
}
const loadClipnodes = (loadmodel, buf, bspVersion) => {
  var size = bspVersion === VERSION["2psb"] || bspVersion === VERSION.bsp2 ? 12 : 8
  var view = new DataView(buf);
  var fileofs = view.getUint32((LUMP.clipnodes << 3) + 4, true);
  var filelen = view.getUint32((LUMP.clipnodes << 3) + 8, true);
  var count = filelen / size;
  loadmodel.clipnodes = [];

  loadmodel.hulls = [];
  loadmodel.hulls[1] = {
    clipnodes: loadmodel.clipnodes,
    firstclipnode: 0,
    lastclipnode: count - 1,
    planes: loadmodel.planes,
    clip_mins: [-16.0, -16.0, -24.0],
    clip_maxs: [16.0, 16.0, 32.0]
  };
  loadmodel.hulls[2] = {
    clipnodes: loadmodel.clipnodes,
    firstclipnode: 0,
    lastclipnode: count - 1,
    planes: loadmodel.planes,
    clip_mins: [-32.0, -32.0, -24.0],
    clip_maxs: [32.0, 32.0, 64.0]
  };

  for (var i = 0; i < count; ++i)
  {
    if (bspVersion === VERSION["2psb"] || bspVersion === VERSION.bsp2) {
      loadmodel.clipnodes[i] = {
        planenum: view.getUint32(fileofs, true),
        children: [view.getInt32(fileofs + 4, true), view.getInt32(fileofs + 8, true)]
      };
    } else {
			//johnfitz -- support clipnodes > 32k
      var out = {
        planenum: view.getUint32(fileofs, true),
        children: [view.getInt16(fileofs + 4, true), view.getInt16(fileofs + 6, true)]
      };
			if (out.children[0] >= count)
				out.children[0] -= 65536;
			if (out.children[1] >= count)
        out.children[1] -= 65536;
      loadmodel.clipnodes[i] = out
    }
    fileofs += size;
  }
}

const makeHull0 = (loadmodel) => {
  var node, child, clipnodes = [], i, out;
  var hull = {
    clipnodes: clipnodes,
    lastclipnode: loadmodel.nodes.length - 1,
    planes: loadmodel.planes,
    clip_mins: [0.0, 0.0, 0.0],
    clip_maxs: [0.0, 0.0, 0.0]
  };
  for (i = 0; i < loadmodel.nodes.length; ++i)
  {
    node = loadmodel.nodes[i];
    out = {planenum: node.planenum, children: []};
    child = node.children[0];
    out.children[0] = child.contents < 0 ? child.contents : child.num;
    child = node.children[1];
    out.children[1] = child.contents < 0 ? child.contents : child.num;
    clipnodes[i] = out;
  }
  loadmodel.hulls[0] = hull;
}

const loadEntities = (loadmodel, buf) => {
  var view = new DataView(buf);
  var fileofs = view.getUint32((LUMP.entities << 3) + 4, true);
  var filelen = view.getUint32((LUMP.entities << 3) + 8, true);
  loadmodel.entities = memstr(new Uint8Array(buf, fileofs, filelen));
}

const loadSubmodels = (loadmodel, buf) => {
  var view = new DataView(buf);
  var fileofs = view.getUint32((LUMP.models << 3) + 4, true);
  var filelen = view.getUint32((LUMP.models << 3) + 8, true);
  var count = filelen >> 6;
  if (count === 0)
    throw new Error('Mod.LoadSubmodels: funny lump size in ' + loadmodel.name);
  loadmodel.submodels = [];

  loadmodel.visleafs = view.getUint32(fileofs + 52, true);
  loadmodel.numleafs = loadmodel.visleafs

  loadmodel.mins = [view.getFloat32(fileofs, true) - 1.0,
    view.getFloat32(fileofs + 4, true) - 1.0,
    view.getFloat32(fileofs + 8, true) - 1.0];
  loadmodel.maxs = [view.getFloat32(fileofs + 12, true) + 1.0,
    view.getFloat32(fileofs + 16, true) + 1.0,
    view.getFloat32(fileofs + 20, true) + 1.0];
  loadmodel.hulls[0].firstclipnode = view.getUint32(fileofs + 36, true);
  loadmodel.hulls[1].firstclipnode = view.getUint32(fileofs + 40, true);
  loadmodel.hulls[2].firstclipnode = view.getUint32(fileofs + 44, true);
  fileofs += 64;

  var i, clipnodes = loadmodel.hulls[0].clipnodes, out;
  for (i = 1; i < count; ++i)
  {
    out = findName('*' + i);
    out.needload = false;
    out.type = TYPE.brush;
    out.submodel = true;
    out.mins = [view.getFloat32(fileofs, true) - 1.0,
      view.getFloat32(fileofs + 4, true) - 1.0,
      view.getFloat32(fileofs + 8, true) - 1.0];
    out.maxs = [view.getFloat32(fileofs + 12, true) + 1.0,
      view.getFloat32(fileofs + 16, true) + 1.0,
      view.getFloat32(fileofs + 20, true) + 1.0];
    out.origin = [view.getFloat32(fileofs + 24, true), view.getFloat32(fileofs + 28, true), view.getFloat32(fileofs + 32, true)];
    out.hulls = [
      {
        clipnodes: clipnodes,
        firstclipnode: view.getUint32(fileofs + 36, true),
        lastclipnode: loadmodel.nodes.length - 1,
        planes: loadmodel.planes,
        clip_mins: [0.0, 0.0, 0.0],
        clip_maxs: [0.0, 0.0, 0.0]
      },
      {
        clipnodes: loadmodel.clipnodes,
        firstclipnode: view.getUint32(fileofs + 40, true),
        lastclipnode: loadmodel.clipnodes.length - 1,
        planes: loadmodel.planes,
        clip_mins: [-16.0, -16.0, -24.0],
        clip_maxs: [16.0, 16.0, 32.0]
      },
      {
        clipnodes: loadmodel.clipnodes,
        firstclipnode: view.getUint32(fileofs + 44, true),
        lastclipnode: loadmodel.clipnodes.length - 1,
        planes: loadmodel.planes,
        clip_mins: [-32.0, -32.0, -24.0],
        clip_maxs: [32.0, 32.0, 64.0]
      }
    ];
    out.textures = loadmodel.textures;
    out.lightdata = loadmodel.lightdata;
    out.faces = loadmodel.faces;
    out.visleafs = view.getUint32(fileofs + 52, true);
    out.firstface = view.getUint32(fileofs + 56, true);
    out.numfaces = view.getUint32(fileofs + 60, true);
    loadmodel.submodels[i - 1] = out;
    fileofs += 64;
  }
}

export const loadBrushModel = (gl: WebGLRenderingContext, buffer) => {
  const loadmodel = {
    type: 0, // BRUSH
    name: 'My Map',
    vertexes: [],
    radius: -1
  } as any

  var version = (new DataView(buffer)).getUint32(0, true);

  switch (version) {
    case VERSION.bsp2:
    case VERSION["2psb"]:
    case VERSION.brush:
      break;
    default:
      throw new Error('Mod.LoadBrushModel: ' +  loadmodel.name  + ' has wrong version number (' + version + ')');
  }

  loadVertexes(loadmodel, buffer);
  loadEdges(loadmodel, buffer, version);
  loadSurfedges(loadmodel, buffer);
  loadTextures(gl, loadmodel, buffer);
  loadLighting(loadmodel, buffer);
  
  loadPlanes(loadmodel, buffer)
  loadTexinfo(loadmodel, buffer);
  loadFaces(loadmodel, buffer, version);
  loadMarksurfaces(loadmodel, buffer, version);
  
  loadVisibility(loadmodel, buffer);
  loadLeafs(loadmodel, buffer, version);
  loadNodes(loadmodel, buffer, version);
  loadClipnodes(loadmodel, buffer, version);
  makeHull0(loadmodel);
  loadEntities(loadmodel, buffer);
  loadSubmodels(loadmodel, buffer);

  var i, vert, mins = [0.0, 0.0, 0.0], maxs = [0.0, 0.0, 0.0];
  for (i = 0; i < loadmodel.vertexes.length; ++i)
  {
    vert = loadmodel.vertexes[i];
    if (vert[0] < mins[0])
      mins[0] = vert[0];
    else if (vert[0] > maxs[0])
      maxs[0] = vert[0];

    if (vert[1] < mins[1])
      mins[1] = vert[1];
    else if (vert[1] > maxs[1])
      maxs[1] = vert[1];

    if (vert[2] < mins[2])
      mins[2] = vert[2];
    else if (vert[2] > maxs[2])
      maxs[2] = vert[2];
  };

  loadmodel.radius = radiusFromBounds(mins, maxs)
  
  
  return loadmodel
}
