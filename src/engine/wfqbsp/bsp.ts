import { setPallet } from './pallete'
import { init as initTextures } from './textures'
import { loadBinary } from '../helpers/asset'
import { loadBrushModel } from './modelLoader'
import { init as initEntities } from './entities'

// const defaultPalleteUrl = require('../../../assets/gfx/palette.lmp')
// const defaultBspUrl = require('../../../assets/bsp/start.bsp')

// TODO fog
// import fog from './fog'
import * as mapAlpha from './mapAlpha'
import { freePrograms } from './program'

const novis = Array.from({length: 1024}, () => 0xff)

const decompressVis = (i, model) => {
  var decompressed = [], c, out = 0, row = (model.numleafs + 7) >> 3;
  if (model.visdata == null)
  {
    for (; row >= 0; --row)
      decompressed[out++] = 0xff;
    return decompressed;
  }
  for (out = 0; out < row; )
  {
    if (model.visdata[i] !== 0)
    {
      decompressed[out++] = model.visdata[i++];
      continue;
    }
    for (c = model.visdata[i + 1]; c > 0; --c)
      decompressed[out++] = 0;
    i += 2;
  }
  return decompressed;
}

export var world = null
export var models = []

export const init = async (gl, bspUrl?, palleteUrl?) => {
  const pallete = await loadBinary(palleteUrl ?? '/assets/gfx/palette.lmp')
  const bsp = await loadBinary(bspUrl ?? '/assets/bsp/e1m1.bsp')

  initTextures(gl)
  setPallet(pallete)

  world = loadBrushModel(gl, bsp)
	// for (var i = 1; i <= world.submodels.length; ++i) {
	// 	models[i + 1] = await mod.forName('*' + i, false);
	// }
	initEntities(world)
  //fog.init
  mapAlpha.init(world)
}

export const pointInLeaf = (p, model) => {
  if (model == null)
    throw new Error('Mod.PointInLeaf: bad model');
  if (model.nodes == null)
    throw new Error('Mod.PointInLeaf: bad model');
  var node = model.nodes[0];
  var normal;
  for (;;)
  {
    if (node.contents < 0)
      return node;
    normal = node.plane.normal;
    if ((p[0] * normal[0] + p[1] * normal[1] + p[2] * normal[2] - node.plane.dist) > 0)
      node = node.children[0];
    else
      node = node.children[1];
  }
}

export const leafPVS = function(leaf, model)
{
  if (leaf === model.leafs[0])
    return novis;
  return decompressVis(leaf.visofs, model);
}

