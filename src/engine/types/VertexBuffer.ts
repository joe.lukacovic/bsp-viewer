export class VertexBuffer {
  gl: WebGL2RenderingContext
  bytes: Uint8Array
  buffer: ArrayBuffer
  position: number
  size: number
  glBuffer: WebGLBuffer
  view: DataView
  
  constructor(gl, size) {
    this.gl = gl
    this.buffer = new ArrayBuffer(size)
    this.bytes = new Uint8Array(this.buffer)
    this.view = new DataView(this.buffer)
    this.position = 0
    this.glBuffer = gl.createBuffer()
  }

  vertexCount (): Number {
    return this.size / 3
  }

  addByte (byte) {
    this.view.setUint8(this.position, byte)
    this.position += 1
  } 

  addByte4 (byte1, byte2, byte3, byte4) {
    this.view.setUint8(this.position, byte1)
    this.view.setUint8(this.position, byte2)
    this.view.setUint8(this.position, byte3)
    this.view.setUint8(this.position, byte4)
    this.position += 4
  } 

  addFloat (float) {
    this.view.setFloat32(this.position, float, true)
    this.position += 4
  }
  addFloat4 (float1, float2, float3, float4) {

    this.view.setFloat32(this.position, float1, true)
    this.view.setFloat32(this.position + 4, float2, true)
    this.view.setFloat32(this.position + 8, float3, true)
    this.view.setFloat32(this.position + 12, float4, true )
    this.position += 16
  }
}