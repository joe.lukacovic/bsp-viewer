import {start as startRender} from './renderLoop'
import {init as initUserInterface} from './userInterface'
import {init as initGlState, gl} from './glstate'
import {init as initBsp} from './wfqbsp/bsp'
import {init as initScene, free as freeScene} from './wfqbsp/scene'

export type ViewerOptions = {
  showOverlay?: boolean
  cameraWobble?: boolean
  sampleInput?: boolean
  origin?: [number, number, number]
}
export const init = async (dom, bspUrl?, palleteUrl?, options?: ViewerOptions) => {
  initGlState(dom)
  if (options?.showOverlay){
    await initUserInterface(gl)
  }
  await initBsp(gl, bspUrl, palleteUrl)

  initScene(gl, options)
  startRender(options)
}

export const free = async () => {
  freeScene(gl)
}