import {fragment as charFragment, vertex as charVertex} from './shader/character'
import {VertexBuffer} from './types/VertexBuffer'
import {createShader} from './helpers/glhelper'
import { loadImage } from './helpers/asset'
import {fps} from './renderLoop'


const displaySize = 100
const fontSize = 32
const vertices = []

const state = {
  vertexBuffer: null,
  texCoordBuffer: null,
  fontTexture: null,
  text: "Hello World!",
  programs: {
    character: null
  },
  textures: {
    charmap: null
  },
  fps: {
    display: 0,
    aggregate: [],
    displayUpdateTime: 0
  }
}

// const state = {
//   vertexBuffer:
// }

const character = (x, y, char) => {
  
}

const calcTexPos = (charNum) => {
  const x = (((charNum % 16) * fontSize) / 512)
  const y = ((Math.floor(charNum/16) * fontSize) / 512)
  const csWidth = (fontSize / 512)

  return [
    x, y + csWidth,
    x + csWidth, y,
    x, y,
    
    x, y + csWidth,
    x + csWidth, y + csWidth,
    x + csWidth, y,
  ]
}

const clearBuffers = () => {
  state.vertexBuffer.position = 0
  state.texCoordBuffer.position = 0
}
const addTexLocation = (charNum) => {
  const locs = calcTexPos(charNum)
  locs.forEach(f => state.texCoordBuffer.addFloat(f))
}

const add2dQuad = (vertexBuffer: VertexBuffer, x, y, width, height) => {
  state.vertexBuffer.addFloat(x)
  state.vertexBuffer.addFloat(y)

  state.vertexBuffer.addFloat(x + width)
  state.vertexBuffer.addFloat(y + height)

  state.vertexBuffer.addFloat(x)
  state.vertexBuffer.addFloat(y + height)

  state.vertexBuffer.addFloat(x)
  state.vertexBuffer.addFloat(y)

  state.vertexBuffer.addFloat(x + width)
  state.vertexBuffer.addFloat(y)

  state.vertexBuffer.addFloat(x + width)
  state.vertexBuffer.addFloat(y + height)
}

const pushCharacterVertex = (vertexBuffer: VertexBuffer, x: number, y: number, size: number, char: number) => {
  add2dQuad(vertexBuffer, x, y, size, size)
  addTexLocation(char)
}

const populateVertexStream = (gl: WebGLRenderingContext, text: String,size: number, x, y) => {
  text.split('').forEach((c, idx) => {
    const code = c.charCodeAt(0)
    pushCharacterVertex(state.vertexBuffer, x + (idx * size), y, size, code)
  })
}

const setupCharacterShader = (gl: WebGLRenderingContext) => {
  state.programs.character = createShader(gl, charVertex, charFragment)
}

const loadCharTexture = async (gl) => {
  const image = await loadImage('/assets/img/berlinfont.png')

  state.textures.charmap = gl.createTexture();
  gl.bindTexture(gl.TEXTURE_2D, state.textures.charmap);

  gl.texImage2D(gl.TEXTURE_2D, 0, gl.RGBA, gl.RGBA, gl.UNSIGNED_BYTE, image);
  gl.texParameterf(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.LINEAR);
  gl.texParameterf(gl.TEXTURE_2D, gl.TEXTURE_MAG_FILTER, gl.LINEAR);
}

export const init = async (gl) => {
  state.vertexBuffer = new VertexBuffer(gl, 8192)
  state.texCoordBuffer = new VertexBuffer(gl, 8192)

  setupCharacterShader(gl)
  //populateVertexStream(gl, state.text, 50, 100, 100)
  loadCharTexture(gl)
}

export const setFps = (gl, newFps) => {
  state.fps.aggregate.push(newFps)
  const dateNow = new Date().getTime()

  if (state.fps.displayUpdateTime < (dateNow - 1000)) {
    state.fps.display = 
      state.fps.aggregate.reduce((agg, val) => agg + val, 0) / state.fps.aggregate.length
    state.fps.aggregate = []
    state.fps.displayUpdateTime = dateNow

    clearBuffers()
  
    populateVertexStream(gl, Math.floor(state.fps.display) + '', 30, 20, gl.canvas.height - 35)
  
    gl.bindBuffer(gl.ARRAY_BUFFER, state.texCoordBuffer.glBuffer);
    gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(state.texCoordBuffer.buffer), gl.STATIC_DRAW);
  }
}

export const draw = (gl) => {
  gl.useProgram(state.programs.character);
  setFps(gl, fps)

  gl.activeTexture(gl.TEXTURE0);
  gl.bindTexture(gl.TEXTURE_2D, state.textures.charmap);
  
  var coord = gl.getAttribLocation(state.programs.character, "a_position");
  gl.enableVertexAttribArray(coord);
  gl.bindBuffer(gl.ARRAY_BUFFER, state.vertexBuffer.glBuffer);
  gl.vertexAttribPointer(coord, 2, gl.FLOAT, false, 0, 0);
  
  gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(state.vertexBuffer.buffer), gl.STATIC_DRAW);
  
  // look up where the texture coordinates need to go.
  var texCoordLocation = gl.getAttribLocation(state.programs.character, "a_texCoord");
  gl.enableVertexAttribArray(texCoordLocation);
  gl.bindBuffer(gl.ARRAY_BUFFER, state.texCoordBuffer.glBuffer)
  gl.vertexAttribPointer(texCoordLocation, 2, gl.FLOAT, false, 0, 0);

  gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(state.texCoordBuffer.buffer), gl.STATIC_DRAW);

  var resolutionUniformLocation = gl.getUniformLocation(state.programs.character, "u_resolution");
  gl.uniform2f(resolutionUniformLocation, gl.canvas.width, gl.canvas.height);

  // gl.bindBuffer(gl.ARRAY_BUFFER, state.vertexBuffer.glBuffer);
  // gl.bindBuffer(gl.ARRAY_BUFFER, vertex_buffer);

  // Draw the triangle
  gl.drawArrays(gl.TRIANGLES, 0, state.vertexBuffer.position);
}