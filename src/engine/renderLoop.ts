import {draw as drawTutorial} from './tutorial'
import {draw as drawInterface} from './userInterface'
import {draw as drawScene} from './wfqbsp/scene'
import {gl, dom} from './glstate'
import * as camera from './wfqbsp/camera'
import { ViewerOptions } from './main'
import { LoaderOptionsPlugin } from '../../node_modules/webpack/types'

export let fps = 0
var lastFrame = 0

var state = {
  viewerOptions: {} as ViewerOptions,
  lastFrame: 0,
  isLooping: false,
  cameraWobble: false,
  sampleInput: true,
  time: 0.0
}

const render = () => {
 
  state.time = Date.now() * 0.001;

  /* Step5: Drawing the required object (triangle) */
  if (state.viewerOptions.sampleInput) {
    camera.sampleInput();
  }
  if (state.viewerOptions.cameraWobble) {
    camera.doWobble(state.time);
  }

  // Clear the canvas
  gl.clearColor(1, 1, 0, 1);
  //gl.cullFace(gl.FRONT);
  gl.blendFuncSeparate(gl.SRC_ALPHA, gl.ONE_MINUS_SRC_ALPHA, gl.ONE, gl.ONE);

  // Enable the depth test
  //gl.enable(gl.DEPTH_TEST); 

  // Clear the color buffer bitx  
  gl.clear(gl.COLOR_BUFFER_BIT);

  // Set the view port
  gl.viewport(0, 0, dom.canvas.width, dom.canvas.height);

  drawScene(gl)
  // drawTutorial(gl)
  if (state.viewerOptions.showOverlay){
    drawInterface(gl)
  }
}

const loop = () => {
  if (!state.isLooping) {
    return
  }
  
  const thisFrame = new Date().getTime()
  const diff = thisFrame - lastFrame
  lastFrame = thisFrame
  
  fps = 1000/diff

  render()
  setTimeout(loop, 5)
}

export const start = (options: ViewerOptions) => {
  state.viewerOptions = options
  state.isLooping = true
  setTimeout(loop, 1)
}

export const stop = () => {
  state.isLooping = false
}