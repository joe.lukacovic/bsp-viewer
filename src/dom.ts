
export const dom = (document) => ({
  docEl: document.documentElement,
  canvas: document.getElementById('render-context') as HTMLCanvasElement,
  addEventHandler: (name, fn) => document.addEventListener(name, fn)
})